import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/auth/login/login_controller.dart';
import 'package:patron_dashboard/const.dart';

class LoginPage extends StatelessWidget {
  final LoginController loginController = Get.put(LoginController());

  LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: purple,
      body: ListView(
        children: [
          Column(
            children: [
              Image.asset(
                "assets/images/logo.png",
                width: 300,
                height: 300,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 300, // Adjust the width as needed
                  height: 70, // Adjust the height as needed
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: TextField(
                      cursorColor: purple,
                      cursorRadius: Radius.circular(20),
                      decoration: InputDecoration(
                        hintText: 'Phone Number',
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          gapPadding: 2,
                        ),
                      ),
                      onChanged: (value) {
                        loginController.phoneNumber(value);
                      },
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Container(
                  width: 300, // Adjust the width as needed
                  height: 70, // Adjust the height as needed
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: TextField(
                      cursorColor: purple,
                      cursorRadius: Radius.circular(20),
                      decoration: InputDecoration(
                        hintText: 'Password',
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          gapPadding: 2,
                        ),
                      ),
                      obscureText: true,
                      onChanged: (value) {
                        loginController.password(value);
                      },
                    ),
                  ),
                ),
                SizedBox(height: 24.0),
                Obx(() {
                  return loginController.isLoading.value
                      ? CircularProgressIndicator()
                      : ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                          ),
                          onPressed: loginController.login,
                          child: Text(
                            'Login',
                            style: TextStyle(color: purple),
                          ),
                        );
                }),
                SizedBox(height: 16.0),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
