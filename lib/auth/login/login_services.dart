// services/auth_service.dart
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:patron_dashboard/confing/confing.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../confing/set_header.dart';

class LoginService extends GetxService {
  Future<Map<String, dynamic>> login(String phoneNumber, String password) async {
    final response = await http.post(
      Uri.parse(ServerConfig.domainNameServer+ServerConfig.login),
      headers: setHeaders(),
      body: {
        'phone_number': phoneNumber,
        'password': password,
      },
    );
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
    var jsonResponse = jsonDecode(response.body);
    var token= jsonResponse['data'];
    await prefs.setString('data', token); // yourTokenValue is the token you get after login
      print(token);
    await prefs.setString(
        'data', token);
      return json.decode(response.body);
    } else {
      throw Exception('Failed to login');
    }
  }
}
