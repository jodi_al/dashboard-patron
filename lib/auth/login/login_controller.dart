// controllers/login_controller.dart
import 'package:get/get.dart';
import 'package:patron_dashboard/auth/login/login_services.dart';

class LoginController extends GetxController {
  final LoginService loginService = Get.put(LoginService());

  var phoneNumber = '1234567890'.obs;
  var password = '123456789'.obs;
  var isLoading = false.obs;

  void login() async {
    isLoading(true);
    try {
      final result = await loginService.login(phoneNumber.value, password.value);
      if (result['success']) {
        Get.offAllNamed('/DashPage');
      } else {
        Get.snackbar('Error', 'Login failed');
      }
    } catch (e) {
      Get.snackbar('Error', e.toString());
    } finally {
      isLoading(false);
    }
  }
}
