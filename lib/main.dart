import 'package:flutter/material.dart';
import 'package:patron_dashboard/const.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/customers/details/customers_details.dart';
import 'package:patron_dashboard/sceens/customers/customers_page.dart';
import 'package:patron_dashboard/sceens/dashboerd/dash_page.dart';
import 'package:patron_dashboard/sceens/log_out_page.dart';
import 'package:patron_dashboard/sceens/messages/massage_page.dart';
import 'package:patron_dashboard/sceens/Offers/offers_page.dart';
import 'package:patron_dashboard/sceens/orders/main_order_page/orders_page.dart';
import 'package:patron_dashboard/sceens/products/create_product_page.dart';
import 'package:patron_dashboard/sceens/products/all_products/all_product_page.dart';
import 'package:patron_dashboard/sceens/reports/reports_page.dart';
import 'package:patron_dashboard/sceens/setting_page.dart';
import 'package:patron_dashboard/sceens/workers/details/worker_details_page.dart';
import 'package:patron_dashboard/sceens/workers/workers_page.dart';
import 'auth/login/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      getPages: [
        GetPage(name: '/DashPage', page: () => DashPage()),
        GetPage(name: '/MassagePage', page: () => MassagePage()),
        GetPage(name: '/CustomersPage', page: () => CustomersPage()),
        GetPage(name: '/OffersPage', page: () => OffersPage()),
        GetPage(name: '/OrdersPage', page: () => OrdersPage()),
        GetPage(name: '/ProductsPage', page: () => AllProductsPage()),
        GetPage(name: '/ReportsPage', page: () => ReportsPage()),
        GetPage(name: '/SettingPage', page: () => SettingPage()),
        GetPage(name: '/WorkersPage', page: () => WorkersPage()),
        GetPage(name: '/LogOutPage', page: () => LogOutPage()),
        GetPage(name: '/LoginPage', page: () => LoginPage()),
     //   GetPage(name: '/ProductDetailsPage', page: () => ProductDetailsPage()),
        GetPage(name: '/ProductFormPage', page: () => CreateProductPage()),
        GetPage(name: '/customerDetails', page: () => CustomerDetailsPage()),
        GetPage(name: '/WorkerDetailsPage', page: () => WorkerDetailsPage()),
      ],transitionDuration: Duration.zero,
      initialRoute: '/LoginPage',
      title: 'Parton Dashboard',
      theme: ThemeData(
        primaryColor: purple,
        canvasColor: light_purple,
        // colorScheme: ColorScheme.fromSeed(seedColor: purple),
        useMaterial3: true,
      ),
    );
  }
}

