import 'package:get/get.dart';
import 'package:patron_dashboard/confing/confing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'model/offer_model.dart';
import 'offers_services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class OffersController extends GetxController {
  var offers = <Offer>[].obs;
  var isLoading = true.obs;
  var errorMessage = ''.obs;
  late String? token;

  @override
  void onInit() {
    super.onInit();
    fetchOffers();
  }

  Future<void> loadToken() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    token = sharedPreferences.getString('data');
  }

  void fetchOffers() async {
    try {
      isLoading(true);
      var fetchedOffers = await OffersService().fetchOffers();
      offers.assignAll(fetchedOffers);
    } catch (e) {
      errorMessage('Failed to load offers');
    } finally {
      isLoading(false);
    }
  }

  void addOffer(Map<String, dynamic> offerData) async {

    await loadToken();
    print('=== $token');

    isLoading.value = true;
    errorMessage.value = '';

    final url = Uri.parse(
        '${ServerConfig.domainNameServer}/api/InsertOffer'); // Replace with your API endpoint

    try {
      final response = await http.post(
        url,
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: offerData,
      );
      print(response.body);
      if (response.statusCode == 200) {
        fetchOffers();
      } else {
        errorMessage.value = 'Failed to add offer. Please try again.';
      }
    } catch (e) {
      errorMessage.value = 'An error occurred: $e';
    } finally {
      isLoading.value = false;
    }
  }
}
