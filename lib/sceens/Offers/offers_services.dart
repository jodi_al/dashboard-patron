import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:patron_dashboard/confing/confing.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/offer_model.dart';

class OffersService extends GetxService {
  var isLoading = true.obs;
  final String apiUrl = ServerConfig.domainNameServer+ServerConfig.getOffers;
  Future<List<Offer>> fetchOffers() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    try {
      String? token = sharedPreferences.getString('data');
      if (token == null) {
        isLoading(false);
        Get.snackbar(
          'Error',
          'Token not found. Please login again.',
          snackPosition: SnackPosition.BOTTOM,
        );
        return [];
      }
      final response = await http.get(
        Uri.parse(apiUrl),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        List<dynamic> data = json.decode(response.body);
        return data.map((offer) => Offer.fromJson(offer)).toList();
      } else {
        throw Exception('Failed to load offers');
      }
    } catch (e) {
      print("Caught Exception: $e");
      Get.snackbar(
        'Error',
        'Failed to load customers. Please try again later.',
        snackPosition: SnackPosition.BOTTOM,
      );
      return [];
    } finally {
      isLoading(false);
    }
  }
}
