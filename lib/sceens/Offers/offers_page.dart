import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:patron_dashboard/componets/Main_view.dart';

import 'Offers_ctrl.dart';
import 'model/offer_model.dart';

class OffersPage extends StatefulWidget {
  @override
  State<OffersPage> createState() => _OffersPageState();
}

class _OffersPageState extends State<OffersPage> {
  final OffersController offersController = Get.put(OffersController());

  final TextEditingController codeController = TextEditingController();
  final TextEditingController discountController = TextEditingController();
  final TextEditingController expireDateController = TextEditingController();
  String _selectedDiscountType = 'percentage'; // Default value

  @override
  Widget build(BuildContext context) {
    return MainView(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('Offers'),
          backgroundColor: Colors.grey[200],
        ),
        body: Obx(() {
          if (offersController.isLoading.value) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(16.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.white),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Add New Offer',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 8),
                        Row(
                          children: [
                            Expanded(
                              child: TextField(
                                controller: codeController,
                                decoration: InputDecoration(labelText: 'Code'),
                              ),
                            ),
                            SizedBox(width: 16),
                            Expanded(
                              child: TextField(
                                controller: discountController,
                                decoration:
                                    InputDecoration(labelText: 'Discount (%)'),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          children: [
                            Expanded(
                              child: DropdownButtonFormField<String>(
                                value: _selectedDiscountType,
                                decoration:
                                    InputDecoration(labelText: 'Discount Type'),
                                items:
                                    ['percentage', 'fixed'].map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    _selectedDiscountType = newValue!;
                                  });
                                },
                              ),
                            ),
                            SizedBox(width: 16),
                            Expanded(
                              child: GestureDetector(
                                onTap: () async {
                                  DateTime? picked = await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2101),
                                  );
                                  if (picked != null) {
                                    String formattedDate =
                                        DateFormat('yyyy-MM-dd').format(picked);
                                    setState(() {
                                      expireDateController.text = formattedDate;
                                    });
                                  }
                                },
                                child: AbsorbPointer(
                                  child: TextField(
                                    controller: expireDateController,
                                    decoration: InputDecoration(
                                        labelText: 'Expire Date'),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 16),
                        Center(
                          child: ElevatedButton(
                            onPressed: () {
                              Map<String, dynamic> offerData = {
                                'code': codeController.text,
                                'discount': discountController.text,
                                'discount_type': _selectedDiscountType,
                                'expire_date': expireDateController.text,
                              };
                              codeController.clear();
                              discountController.clear();
                              setState(() {
                                _selectedDiscountType = 'percentage';
                              });
                              expireDateController.clear();
                              offersController.addOffer(offerData);
                            },
                            child: Text('Add Offer'),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(16.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.white),
                      ),
                      child: Column(
                        children: [
                          Container(
                            height: 75,
                            color: Color(0xff936b9f),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 30.0),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text('Code',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                      child: Text('Discount',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                      child: Text('Discount Type',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                      child: Text('Expire Date',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Container(
                                width: MediaQuery.of(context).size.width -
                                    64, // Adjusted for padding
                                child: DataTable(
                                  columns: [
                                    DataColumn(label: Text('')),
                                    DataColumn(label: Text('')),
                                    DataColumn(label: Text('')),
                                    DataColumn(label: Text('')),
                                  ],
                                  rows: offersController.offers.map((offer) {
                                    return DataRow(
                                      cells: [
                                        DataCell(Text(offer.code)),
                                        DataCell(Text('${offer.discount}%')),
                                        DataCell(Text(offer.discountType)),
                                        DataCell(Text(offer.expireDate)),
                                      ],
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        }),
      ),
    );
  }
}
