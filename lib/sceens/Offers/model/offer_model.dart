class Offer {
  final int id;
  final String code;
  final String discount;
  final String discountType;
  final String expireDate;

  Offer({
    required this.id,
    required this.code,
    required this.discount,
    required this.discountType,
    required this.expireDate,
  });
  Map<String, dynamic> toJson() {
      return {
        'code': code,
        'discount': discount,
        'discount_type': discountType,
        'expire_date': expireDate,
      };
    }
  factory Offer.fromJson(Map<String, dynamic> json) {
    return Offer(
      id: json['id'],
      code: json['code'],
      discount: json['discount'],
      discountType: json['discount_type'],
      expireDate: json['expire_date'],
    );
  }
}