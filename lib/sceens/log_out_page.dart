import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/const.dart';

import '../componets/Main_view.dart';

class LogOutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainView(child: DialogExample());
  }
}
class DialogExample extends StatelessWidget {
  const DialogExample({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(backgroundColor: Colors.white, surfaceTintColor: Colors.white,shadowColor: Colors.white.withOpacity(0.3),
          title: Text('Are you sure you want to log out?'),
          icon: Icon(Icons.warning_amber, size: 100,color: purple,),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Log out'),
              onPressed: () {
                Get.offAllNamed('/LoginPage');
              },
            ),
          ],
    );
  }
}
