import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../../confing/confing.dart';

import 'models/customer.dart';

class CustomerService extends GetxService {
  var isLoading = true.obs;

  Future<List<CustomerData>> getAllCustomers() async {
    isLoading(true);
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      String? token = sharedPreferences.getString('data');
      if (token == null) {
        isLoading(false);
        Get.snackbar(
          'Error',
          'Token not found. Please login again.',
          snackPosition: SnackPosition.BOTTOM,
        );
        return [];
      }
      final response = await http.get(
        Uri.parse(ServerConfig.domainNameServer + ServerConfig.getAllCustomers),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200) {
        List jsonResponse = json.decode(response.body)['data'];
        return jsonResponse
            .map<CustomerData>((customer) => CustomerData.fromJson(customer))
            .toList();
      } else {
        throw Exception('Failed to load customers');
      }
    } catch (e) {
      print("Caught Exception: $e");
      Get.snackbar(
        'Error',
        'Failed to load customers. Please try again later.',
        snackPosition: SnackPosition.BOTTOM,
      );
      return [];
    } finally {
      isLoading(false);
    }
  }
}
