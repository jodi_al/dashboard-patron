import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'componets/customers_cards.dart';
import 'customer_ctr.dart';

class CustomersPage extends StatelessWidget {
  final CustomerController customerController = Get.put(CustomerController());

  @override
  Widget build(BuildContext context) {
    return MainView(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Obx(() => CustomerCard(
                      title: 'Total Customers',
                      value: customerController.customers.length.toString(),
                      icon: Icons.people,
                    )),
                    CustomerCard(
                      title: 'Related to company',
                      value: '1,893', // Update this with the relevant data
                      icon: Icons.business,
                    ),
                    CustomerCard(
                      title: 'Active Now',
                      value: '189', // Update this with the relevant data
                      icon: Icons.computer,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'All Customers',
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 10),
                      Obx(() {
                        if (customerController.isLoading.value) {
                          return Center(child: CircularProgressIndicator());
                        } else if (customerController.errorMessage.isNotEmpty) {
                          return Center(
                              child:
                              Text(customerController.errorMessage.value));
                        } else {
                          return SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: DataTable(
                              columns: const [
                                DataColumn(
                                    label: Text(
                                      'Customer Name',
                                      style: TextStyle(color: Colors.grey),
                                    )),
                                DataColumn(
                                    label: Text(
                                      'Phone Number',
                                      style: TextStyle(color: Colors.grey),
                                    )),
                                DataColumn(
                                    label: Text(
                                      'Email',
                                      style: TextStyle(color: Colors.grey),
                                    )),
                                DataColumn(
                                    label: Text(
                                      'Country',
                                      style: TextStyle(color: Colors.grey),
                                    )),
                                DataColumn(
                                    label: Text(
                                      'Status',
                                      style: TextStyle(color: Colors.grey),
                                    )),
                                DataColumn(
                                    label: Text(
                                      'Details',
                                      style: TextStyle(color: Colors.grey),
                                    ))
                              ],
                              rows:
                              customerController.customers.map((customer) {
                                return DataRow(
                                  cells: [
                                    DataCell(
                                      Text(customer.fullName),
                                    ),
                                    DataCell(Text(customer.phoneNumber)),
                                    DataCell(Text(customer.email)),
                                    DataCell(Text(customer.address)),
                                    DataCell(Text(customer.isStore == 1
                                        ? 'Store'
                                        : 'Individual')),
                                    DataCell(TextButton(
                                      onPressed: () {
                                        Get.toNamed('/customerDetails',
                                            arguments: customer.id);
                                      },
                                      child: Text('More'),
                                    ))
                                  ],
                                );
                              }).toList(),
                            ),
                          );
                        }
                      }),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
