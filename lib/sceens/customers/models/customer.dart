// models/customer.dart

import 'dart:convert';

Customer customerFromJson(String str) => Customer.fromJson(json.decode(str));

String customerToJson(Customer data) => json.encode(data.toJson());

class Customer {
  List<CustomerData> data;

  Customer({
    required this.data,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    data: List<CustomerData>.from(json["data"].map((x) => CustomerData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class CustomerData {
  int id;
  int roleId;
  String fullName;
  String email;
  Avatar avatar;
  String phoneNumber;
  String address;
  dynamic image;
  int isStore;

  CustomerData({
    required this.id,
    required this.roleId,
    required this.fullName,
    required this.email,
    required this.avatar,
    required this.phoneNumber,
    required this.address,
    required this.image,
    required this.isStore,
  });

  factory CustomerData.fromJson(Map<String, dynamic> json) => CustomerData(
    id: json["id"],
    roleId: json["role_id"],
    fullName: json["full_name"],
    email: json["email"],
    avatar: avatarValues.map[json["avatar"]]!,
    phoneNumber: json["phone_number"],
    address: json["address"],
    image: json["image"],
    isStore: json["is_store"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "full_name": fullName,
    "email": email,
    "avatar": avatarValues.reverse[avatar],
    "phone_number": phoneNumber,
    "address": address,
    "image": image,
    "is_store": isStore,
  };
}

enum Avatar {
  USERS_DEFAULT_PNG
}

final avatarValues = EnumValues({
  "users/default.png": Avatar.USERS_DEFAULT_PNG
});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
