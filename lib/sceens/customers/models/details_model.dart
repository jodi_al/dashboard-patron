// To parse this JSON data, do
//
//     final customerDetails = customerDetailsFromJson(jsonString);

import 'dart:convert';

CustomerDetails customerDetailsFromJson(String str) => CustomerDetails.fromJson(json.decode(str));

String customerDetailsToJson(CustomerDetails data) => json.encode(data.toJson());

class CustomerDetails {
  Data data;

  CustomerDetails({
    required this.data,
  });

  factory CustomerDetails.fromJson(Map<String, dynamic> json) => CustomerDetails(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  int id;
  int roleId;
  String fullName;
  String email;
  String avatar;
  String phoneNumber;
  String address;
  dynamic image;
  int isStore;

  Data({
    required this.id,
    required this.roleId,
    required this.fullName,
    required this.email,
    required this.avatar,
    required this.phoneNumber,
    required this.address,
    required this.image,
    required this.isStore,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    roleId: json["role_id"],
    fullName: json["full_name"],
    email: json["email"],
    avatar: json["avatar"],
    phoneNumber: json["phone_number"],
    address: json["address"],
    image: json["image"],
    isStore: json["is_store"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "full_name": fullName,
    "email": email,
    "avatar": avatar,
    "phone_number": phoneNumber,
    "address": address,
    "image": image,
    "is_store": isStore,
  };
}
