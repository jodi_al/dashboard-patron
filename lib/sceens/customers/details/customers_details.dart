// pages/customer_details_page.dart
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/confing/confing.dart';
import 'package:patron_dashboard/const.dart';

import 'customer_details_controller.dart';

class CustomerDetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final int customerId = Get.arguments ?? 0;
    final CustomerDetailsController customerDetailsController = Get.put(CustomerDetailsController());

    // Fetch customer details if they are not already fetched
    if (customerDetailsController.customerDetails.value == null || customerDetailsController.customerDetails.value!.data.id != customerId) {
      customerDetailsController.fetchCustomerDetails(customerId);
    }

    return MainView(
      key: UniqueKey(),
      child: Padding(
        padding: const EdgeInsets.all(defaultPadding),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text(
                'Customer Details',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            body: Obx(() {
              if (customerDetailsController.isLoading.value) {
                return Center(child: CircularProgressIndicator());
              } else if (customerDetailsController.errorMessage.isNotEmpty) {
                return Center(child: Text(customerDetailsController.errorMessage.value));
              } else if (customerDetailsController.customerDetails.value != null) {
                var customer = customerDetailsController.customerDetails.value!.data;
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          customer.avatar.isNotEmpty
                              ? Image.network(
                            '${ServerConfig.domainNameServer}${customer.avatar}',
                            width: 100,
                            height: 100,
                            errorBuilder: (context, error, stackTrace) {
                              return Icon(Icons.account_circle, size: 100);
                            },
                          )
                              : Icon(Icons.account_circle, size: 100),
                          SizedBox(width: 16),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(customer.fullName,
                                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                              Text(customer.email),
                              Text(customer.phoneNumber),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Text('Address: ${customer.address}', style: TextStyle(fontSize: 18)),
                      SizedBox(height: 16),
                      Text('Role ID: ${customer.roleId}', style: TextStyle(fontSize: 18)),
                      SizedBox(height: 16),
                      Text('Is Store: ${customer.isStore == 1 ? 'Yes' : 'No'}', style: TextStyle(fontSize: 18)),
                      TextButton(
                        onPressed: () {
                          Get.offNamed('/CustomersPage');
                        },
                        child: Text(
                          'Go back',
                          style: TextStyle(fontSize: 18, color: purple1),
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(child: Text('Customer not found'));
              }
            }),
          ),
        ),
      ),
    );
  }
}
