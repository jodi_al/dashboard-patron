// controllers/customer_details_controller.dart
import 'package:get/get.dart';
import '../models/details_model.dart';
import 'customer_details_service.dart';

class CustomerDetailsController extends GetxController {
  final CustomerDetailsService customerDetailsService = Get.put(CustomerDetailsService());
  var customerDetails = Rxn<CustomerDetails>();
  var isLoading = false.obs;
  var errorMessage = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> fetchCustomerDetails(int id) async {
    isLoading(true);
    errorMessage('');
    try {
      CustomerDetails details = await customerDetailsService.getCustomerDetails(id);
      customerDetails.value = details;
      print('Customer details fetched: ${customerDetails.value}');
    } catch (e) {
      errorMessage(e.toString());
      print('Error fetching customer details: $e');
    } finally {
      isLoading(false);
    }
  }
}
