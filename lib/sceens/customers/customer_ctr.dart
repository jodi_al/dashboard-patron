import 'package:get/get.dart';
import 'customer_srevices.dart';
import 'models/customer.dart';

class CustomerController extends GetxController {
  final CustomerService customerService = Get.put(CustomerService());

  var customers = <CustomerData>[].obs;  // Use CustomerData as it represents the customer data
  var isLoading = false.obs;
  var errorMessage = ''.obs;

  @override
  void onInit() {
    fetchAllCustomers();
    super.onInit();
  }

  void fetchAllCustomers() async {
    isLoading(true);
    errorMessage('');
    try {
      customers.value = await customerService.getAllCustomers();
    } catch (e) {
      errorMessage(e.toString());
    } finally {
      isLoading(false);
    }
  }
}
