// pages/order_details_page.dart
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';

import 'details_ctrl.dart';

class OrderDetailsPage extends StatelessWidget {
  final int orderId;
  final OrderDetailsController orderDetailsController =
      Get.put(OrderDetailsController());

  OrderDetailsPage({required this.orderId});

  @override
  Widget build(BuildContext context) {
    // Fetch order details when the page is opened
    orderDetailsController.fetchOrderDetails(orderId);

    return MainView(
      key: UniqueKey(),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: () {
                  Get.offNamed('/OrdersPage');
                },
                icon: Icon(Icons.arrow_back_sharp),
              ),
              title: Text('Order Details'),
            ),
            body: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Obx(() {
                if (orderDetailsController.isLoading.value) {
                  return Center(child: CircularProgressIndicator());
                } else if (orderDetailsController.orderDetails.value == null) {
                  return Center(
                      child: Text('No details available for this order'));
                } else {
                  var order = orderDetailsController.orderDetails.value!;
                  return ListView(
                    children: [
                      Card(
                        key: ValueKey(order.id),
                        margin: EdgeInsets.symmetric(vertical: 10),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Order ID: ${order.id}',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 10),
                              Text('Orderable Type: ${order.orderableType}'),
                              Text('Orderable ID: ${order.orderableId}'),
                              Text('Price Info: ${order.priceInfo}'),
                              Text('Is Approve: ${order.isApprove}'),
                              Text(
                                  'Is Approve Custom: ${order.isApproveCustom}'),
                              Text('Deadline: ${order.deadline ?? "N/A"}'),
                              Text('Status Order: ${order.statusOrder}'),
                            ],
                          ),
                        ),
                      ),
                      Card(
                        key: ValueKey(order.orderable.id),
                        margin: EdgeInsets.symmetric(vertical: 10),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Orderable Details',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 10),
                              Text('Orderable ID: ${order.orderable.id}'),
                              Text('User ID: ${order.orderable.userId}'),
                              Text('Is Ordered: ${order.orderable.isOrdered}'),
                              SizedBox(height: 10),
                              Text('User Info',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                              Text(
                                  'User Name: ${order.orderable.user.fullName}'),
                              Text('User Email: ${order.orderable.user.email}'),
                              Text(
                                  'Phone Number: ${order.orderable.user.phoneNumber}'),
                              Text('Address: ${order.orderable.user.address}'),
                            ],
                          ),
                        ),
                      ),
                      ...order.orderable.details.map((detail) {
                        return Card(
                          key: ValueKey(detail.id),
                          margin: EdgeInsets.symmetric(vertical: 10),
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Detail ID: ${detail.id}',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(height: 10),
                                Text('Color: ${detail.color.name}'),
                                Text('Size: ${detail.size.name}'),
                                SizedBox(height: 10),
                                Text('Product Info',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                Text('Product Name: ${detail.product.name}'),
                                Text(
                                    'Product Description: ${detail.product.description}'),
                                Text('Product Code: ${detail.product.code}'),
                              ],
                            ),
                          ),
                        );
                      }).toList(),
                    ],
                  );
                }
              }),
            ),
          ),
        ),
      ),
    );
  }
}
