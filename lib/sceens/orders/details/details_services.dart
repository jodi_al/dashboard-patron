// services/order_details_service.dart
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:patron_dashboard/confing/confing.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/details_model.dart';


class OrderDetailsService extends GetxService {
  var isLoading = true.obs;

  Future<Data?> getOrderDetails(int orderId) async {
    isLoading(true);
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      String? token = sharedPreferences.getString('data');
      if (token == null) {
        isLoading(false);
        Get.snackbar(
          'Error',
          'Token not found. Please login again.',
          snackPosition: SnackPosition.BOTTOM,
        );
        return null;
      }

      final response = await http.get(
        Uri.parse('${ServerConfig.domainNameServer+ServerConfig.ShowOrder}$orderId'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      print("Response Status: ${response.statusCode}");
      print("Response Body: ${response.body}");

      if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        if (jsonResponse is Map<String, dynamic> && jsonResponse.containsKey('data')) {
          return Data.fromJson(jsonResponse['data']);
        } else {
          throw Exception('Unexpected JSON structure');
        }
      } else {
        throw Exception('Failed to load order details with status code: ${response.statusCode}');
      }
    } catch (e) {
      print("Caught Exception: $e");
      Get.snackbar(
        'Error',
        'Failed to load order details. Please try again later.',
        snackPosition: SnackPosition.BOTTOM,
      );
      return null;
    } finally {
      isLoading(false);
    }
  }
}
