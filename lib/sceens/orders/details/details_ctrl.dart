// controllers/order_details_controller.dart
import 'package:get/get.dart';

import '../models/details_model.dart';
import 'details_services.dart';


class OrderDetailsController extends GetxController {
  var isLoading = false.obs;
  var orderDetails = Rxn<Data>();
  OrderDetailsService orderDetailsService = OrderDetailsService();

  @override
  void onInit() {
    super.onInit();
  }

  void fetchOrderDetails(int orderId) async {
    try {
      isLoading(true);
      var details = await orderDetailsService.getOrderDetails(orderId);
      if (details != null) {
        orderDetails.value = details;
        print("Order details fetched for order ID $orderId");
      }
    } finally {
      isLoading(false);
    }
  }

}
