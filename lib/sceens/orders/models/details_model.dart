// To parse this JSON data, do
//
//     final ordersDetails = ordersDetailsFromJson(jsonString);

import 'dart:convert';

OrdersDetails ordersDetailsFromJson(String str) => OrdersDetails.fromJson(json.decode(str));

String ordersDetailsToJson(OrdersDetails data) => json.encode(data.toJson());

class OrdersDetails {
  Data data;

  OrdersDetails({
    required this.data,
  });

  factory OrdersDetails.fromJson(Map<String, dynamic> json) => OrdersDetails(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  int id;
  String orderableType;
  int orderableId;
  String priceInfo;
  int isApprove;
  int isApproveCustom;
  DateTime deadline;
  String statusOrder;
  Orderable orderable;

  Data({
    required this.id,
    required this.orderableType,
    required this.orderableId,
    required this.priceInfo,
    required this.isApprove,
    required this.isApproveCustom,
    required this.deadline,
    required this.statusOrder,
    required this.orderable,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    orderableType: json["orderable_type"],
    orderableId: json["orderable_id"],
    priceInfo: json["price_info"],
    isApprove: json["is_approve"],
    isApproveCustom: json["is_approve_custom"],
    deadline: DateTime.parse(json["deadline"]),
    statusOrder: json["status_order"],
    orderable: Orderable.fromJson(json["orderable"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "orderable_type": orderableType,
    "orderable_id": orderableId,
    "price_info": priceInfo,
    "is_approve": isApprove,
    "is_approve_custom": isApproveCustom,
    "deadline": deadline.toIso8601String(),
    "status_order": statusOrder,
    "orderable": orderable.toJson(),
  };
}

class Orderable {
  int id;
  int userId;
  int isOrdered;
  User user;
  List<OrdersDetail> details;

  Orderable({
    required this.id,
    required this.userId,
    required this.isOrdered,
    required this.user,
    required this.details,
  });

  factory Orderable.fromJson(Map<String, dynamic> json) => Orderable(
    id: json["id"],
    userId: json["user_id"],
    isOrdered: json["is_ordered"],
    user: User.fromJson(json["user"]),
    details: List<OrdersDetail>.from(json["details"].map((x) => OrdersDetail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "is_ordered": isOrdered,
    "user": user.toJson(),
    "details": List<dynamic>.from(details.map((x) => x.toJson())),
  };
}

class OrdersDetail {
  int id;
  Color color;
  Color size;
  Product product;

  OrdersDetail({
    required this.id,
    required this.color,
    required this.size,
    required this.product,
  });

  factory OrdersDetail.fromJson(Map<String, dynamic> json) => OrdersDetail(
    id: json["id"],
    color: Color.fromJson(json["color"]),
    size: Color.fromJson(json["size"]),
    product: Product.fromJson(json["product"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "color": color.toJson(),
    "size": size.toJson(),
    "product": product.toJson(),
  };
}

class Color {
  int id;
  String name;

  Color({
    required this.id,
    required this.name,
  });

  factory Color.fromJson(Map<String, dynamic> json) => Color(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}

class Product {
  int id;
  String name;
  String description;
  String code;
  String basicImage;
  int isSold;
  List<dynamic> shapes;
  List<Color> fabrics;
  List<dynamic> categories;

  Product({
    required this.id,
    required this.name,
    required this.description,
    required this.code,
    required this.basicImage,
    required this.isSold,
    required this.shapes,
    required this.fabrics,
    required this.categories,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    name: json["name"],
    description: json["description"],
    code: json["code"],
    basicImage: json["basic_image"],
    isSold: json["is_sold"],
    shapes: List<dynamic>.from(json["shapes"].map((x) => x)),
    fabrics: List<Color>.from(json["fabrics"].map((x) => Color.fromJson(x))),
    categories: List<dynamic>.from(json["categories"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
    "code": code,
    "basic_image": basicImage,
    "is_sold": isSold,
    "shapes": List<dynamic>.from(shapes.map((x) => x)),
    "fabrics": List<dynamic>.from(fabrics.map((x) => x.toJson())),
    "categories": List<dynamic>.from(categories.map((x) => x)),
  };
}

class User {
  int id;
  int roleId;
  String fullName;
  String email;
  String avatar;
  String phoneNumber;
  String address;
  dynamic image;
  int isStore;

  User({
    required this.id,
    required this.roleId,
    required this.fullName,
    required this.email,
    required this.avatar,
    required this.phoneNumber,
    required this.address,
    required this.image,
    required this.isStore,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    roleId: json["role_id"],
    fullName: json["full_name"],
    email: json["email"],
    avatar: json["avatar"],
    phoneNumber: json["phone_number"],
    address: json["address"],
    image: json["image"],
    isStore: json["is_store"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "full_name": fullName,
    "email": email,
    "avatar": avatar,
    "phone_number": phoneNumber,
    "address": address,
    "image": image,
    "is_store": isStore,
  };
}
