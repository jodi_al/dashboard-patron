import 'dart:convert';

Orders ordersFromJson(String str) => Orders.fromJson(json.decode(str));

String ordersToJson(Orders data) => json.encode(data.toJson());

class Orders {
  List<Datum> data;

  Orders({
    required this.data,
  });

  factory Orders.fromJson(Map<String, dynamic> json) => Orders(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int id;
  String orderableType;
  int orderableId;
  String priceInfo;
  int isApprove;
  int isApproveCustom;
  DateTime? deadline;
  String statusOrder;
  Orderable orderable;

  Datum({
    required this.id,
    required this.orderableType,
    required this.orderableId,
    required this.priceInfo,
    required this.isApprove,
    required this.isApproveCustom,
    required this.deadline,
    required this.statusOrder,
    required this.orderable,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    orderableType: json["orderable_type"],
    orderableId: json["orderable_id"],
    priceInfo: json["price_info"],
    isApprove: json["is_approve"],
    isApproveCustom: json["is_approve_custom"],
    deadline: json["deadline"] == null ? null : DateTime.parse(json["deadline"]),
    statusOrder: json["status_order"],
    orderable: Orderable.fromJson(json["orderable"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "orderable_type": orderableType,
    "orderable_id": orderableId,
    "price_info": priceInfo,
    "is_approve": isApprove,
    "is_approve_custom": isApproveCustom,
    "deadline": deadline?.toIso8601String(),
    "status_order": statusOrder,
    "orderable": orderable.toJson(),
  };
}

class Orderable {
  int id;
  int userId;
  int isOrdered;
  User user;
  List<Detail> details;

  Orderable({
    required this.id,
    required this.userId,
    required this.isOrdered,
    required this.user,
    required this.details,
  });

  factory Orderable.fromJson(Map<String, dynamic> json) => Orderable(
    id: json["id"],
    userId: json["user_id"],
    isOrdered: json["is_ordered"],
    user: User.fromJson(json["user"]),
    details: List<Detail>.from(json["details"].map((x) => Detail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "is_ordered": isOrdered,
    "user": user.toJson(),
    "details": List<dynamic>.from(details.map((x) => x.toJson())),
  };
}

class Detail {
  int id;
  Color color;
  Color size;
  Product product;

  Detail({
    required this.id,
    required this.color,
    required this.size,
    required this.product,
  });

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
    id: json["id"],
    color: Color.fromJson(json["color"]),
    size: Color.fromJson(json["size"]),
    product: Product.fromJson(json["product"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "color": color.toJson(),
    "size": size.toJson(),
    "product": product.toJson(),
  };
}

class Color {
  int id;
  String name;

  Color({
    required this.id,
    required this.name,
  });

  factory Color.fromJson(Map<String, dynamic> json) => Color(
      id: json["id"],
      name: json["name"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}

class Product {
  int id;
  String name;
  String description;
  String basicImage;
  String code;

  Product({
    required this.id,
    required this.name,
    required this.description,
    required this.basicImage,
    required this.code,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    name: json["name"],
    description: json["description"],
    basicImage: json["basic_image"],
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
    "basic_image": basicImage,
    "code": code,
  };
}

class User {
  int id;
  String fullName;

  User({
    required this.id,
    required this.fullName,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    fullName: json["full_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "full_name": fullName,
  };
}
