import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/const.dart';
import '../details/order_details.dart';
import 'order_ctrl.dart';

class OrdersPage extends StatefulWidget {
  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  final OrderController orderController = Get.put(OrderController());
  @override
  void initState() {
    super.initState();
    orderController.fetchOrders(
        status: ''); // Fetch orders when the page initializes
  }
  final RxString selectedFilter = 'All'.obs;
  int selectedPage = 1;

  @override
  Widget build(BuildContext context) {
    return MainView(
      child: Scaffold(

        body: Padding(
          padding: const EdgeInsets.only(bottom:16 ,left: 20,right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Filters Row
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            // Implement add order logic here
                          },
                          child: Text('+ Add Order'),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color(0xff936b9f),
                            foregroundColor: Colors.white,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: ElevatedButton(
                        onPressed: () {
                          orderController.fetchOrders(status: 'All');
                          selectedFilter.value = 'All';
                          setState(() {});
                        },
                        child: Text(
                          'All',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(120, 50),
                          backgroundColor: selectedFilter.value == ''
                              ? Color.fromARGB(255, 109, 78, 119)
                              : Color(0xff936b9f),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: ElevatedButton(
                        onPressed: () {
                          orderController.fetchOrders(status: 'Pending');
                          selectedFilter.value = 'Pending';
                          setState(() {});
                        },
                        child: Text(
                          'Pending',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(120, 50),
                          backgroundColor: selectedFilter.value == 'Pending'
                              ? Color.fromARGB(255, 109, 78, 119)
                              : Color(0xff936b9f),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: ElevatedButton(
                        onPressed: () {
                          orderController.fetchOrders(
                              status: 'Waiting for start');
                          selectedFilter.value = 'Waiting for start';
                          setState(() {});
                        },
                        child: Text(
                          'Waiting for start',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(150, 50),
                          backgroundColor:
                              selectedFilter.value == 'Waiting for start'
                                  ? Color.fromARGB(255, 109, 78, 119)
                                  : Color(0xff936b9f),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: ElevatedButton(
                        onPressed: () {
                          orderController.fetchOrders(status: 'Started');
                          selectedFilter.value = 'Started';
                          setState(() {});
                        },
                        child: Text(
                          'Started',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(120, 50),
                          backgroundColor: selectedFilter.value == 'Started'
                              ? Color.fromARGB(255, 109, 78, 119)
                              : Color(0xff936b9f),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: ElevatedButton(
                        onPressed: () {
                          orderController.fetchOrders(status: 'In Progress');
                          selectedFilter.value = 'In Progress';
                          setState(() {});
                        },
                        child: Text(
                          'In Progress',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(150, 50),
                          backgroundColor: selectedFilter.value == 'In Progress'
                              ? Color.fromARGB(255, 109, 78, 119)
                              : Color(0xff936b9f),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: ElevatedButton(
                        onPressed: () {
                          orderController.fetchOrders(status: 'Finished');
                          selectedFilter.value = 'Finished';
                          setState(() {});
                        },
                        child: Text(
                          'Finished',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(120, 50),
                          backgroundColor: selectedFilter.value == 'Finished'
                              ? Color.fromARGB(255, 109, 78, 119)
                              : Color(0xff936b9f),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              // Main Content Area
              Expanded(
                child: Obx(() {
                  if (orderController.isLoading.value) {
                    return Center(child: CircularProgressIndicator());
                  } else if (orderController.orderList.isEmpty) {
                    return Center(child: Text('No orders available'));
                  } else {
                    return Column(
                      children: [
                        // Table Headers
                        Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 1,
                                  child: Text('ID',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Text('Name',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Text('Address',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 1,
                                  child: Text('Price',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Text('Deadline',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Text('Status',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 1,
                                  child: Text('Approve',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 1,
                                  child: Text('Actions',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                            ],
                          ),
                        ),
                        Expanded(
                          child: ListView(
                            children: orderController.orderList.map((order) {
                              return Container(
                                margin: EdgeInsets.only(top: 10),
                                padding: EdgeInsets.all(16),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 2,
                                      blurRadius: 5,
                                      offset: Offset(0, 3),
                                    ),
                                  ],
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 1, child: Text('${order.id}')),
                                    Expanded(
                                        flex: 2,
                                        child: Text(
                                            order.orderable.user.fullName)),
                                    Expanded(
                                        flex: 2,
                                        child: Text(order.orderable.user
                                            .fullName)), // Assuming address is available
                                    Expanded(
                                        flex: 1, child: Text(order.priceInfo)),
                                    Expanded(
                                        flex: 2,
                                        child: Text('${order.deadline}')),
                                    Expanded(
                                        flex: 2,
                                        child: Text(order.statusOrder)),
                                    Expanded(
                                        flex: 1,
                                        child: Text(order.isApprove == 1
                                            ? 'yes'
                                            : 'no')),
                                    Expanded(
                                      flex: 1,
                                      child: IconButton(
                                        icon: Icon(Icons.edit,
                                            color: Color(0xff936b9f)),
                                        onPressed: () {
                                          Get.to(
                                              OrderDetailsPage(
                                                  orderId: order.id),
                                              arguments: order.id);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    );
                  }
                }),
              ),
              // Pagination Buttons
              SizedBox(height: 16),
              Row(
                children: List.generate(4, (index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          selectedPage = index + 1;
                        });
                        // Implement pagination logic here
                      },
                      child: Text('${index + 1}'),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: selectedPage == index + 1
                            ? Color.fromARGB(255, 109, 78, 119)
                            : Color(0xff936b9f),
                        foregroundColor: Colors.white,
                      ),
                    ),
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
