import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../confing/confing.dart';
import '../models/oeder_model.dart';


class OrderService extends GetxService {
  var isLoading = true.obs;

  Future<Orders?> getAllOrdersForAdmin(String? status) async {
    isLoading(true);
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      String? token = sharedPreferences.getString('data');
      if (token == null) {
        isLoading(false);
        Get.snackbar(
          'Error',
          'Token not found. Please login again.',
          snackPosition: SnackPosition.BOTTOM,
        );
        return null;
      }

      final response = await http.post(
        Uri.parse('${ServerConfig.domainNameServer + ServerConfig.AllOrderForAdmin}?status=$status'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      print("Response Status: ${response.statusCode}");
      print("Response Body: ${response.body}");

      if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        if (jsonResponse is Map<String, dynamic> && jsonResponse.containsKey('data')) {
          return Orders.fromJson(jsonResponse);
        } else {
          throw Exception('Unexpected JSON structure');
        }
      } else {
        throw Exception('Failed to load orders with status code: ${response.statusCode}');
      }
    } catch (e) {
      print("Caught Exception: $e");
      Get.snackbar(
        'Error',
        'Failed to load orders. Please try again later.',
        snackPosition: SnackPosition.BOTTOM,
      );
      return null;
    } finally {
      isLoading(false);
    }
  }

  void confirmOrder(int orderId, int price, String date) async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      isLoading(false);
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      return;
    }

    try {
      final response = await http.post(
        Uri.parse(ServerConfig.domainNameServer + ServerConfig.ConfirmOrder),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: {
          'order_id': orderId.toString(),
          'price_info': price.toString(),
          'deadline': date,
        },
      );

      if (response.statusCode == 200) {
        var jsonResponse = json.decode(response.body);
        if (jsonResponse is Map<String, dynamic> && jsonResponse.containsKey('data')) {
          // Successfully confirmed the order
        }
      } else {
        throw Exception('Failed to confirm order');
      }
    } catch (e) {
      print("Error confirming order: $e");
    }
  }
}
