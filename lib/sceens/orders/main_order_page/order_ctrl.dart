import 'package:get/get.dart';
import '../models/oeder_model.dart';
import 'order_services.dart';

class OrderController extends GetxController {
  var isLoading = false.obs;
  var orderList = <Datum>[].obs;
  OrderService orderService = OrderService();
  var orderDetails = Rxn<Datum>();

  @override
  void onInit() {
    super.onInit();
    fetchOrders(status: 'All');
  }

  void fetchOrders({String? status}) async {
    try {
      isLoading(true);
      var orders = await orderService.getAllOrdersForAdmin(status);
      if (orders != null && orders.data.isNotEmpty) {
        orderList.value = orders.data;
        print("Orders fetched: ${orderList.length}");
      } else {
        orderList.clear(); // Clear the list if no orders are fetched
        print("No orders fetched");
      }
    } catch (e) {
      print("Error fetching orders: $e");
    } finally {
      isLoading(false);
    }
  }
}
