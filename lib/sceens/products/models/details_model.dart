class ProductDetail {
  final int productId;
  final String productName;
  final String code;
  final String productDescription;
  final String productImage;
  final bool isSold;
  final List<Detail> details;
  final List<String> images;

  ProductDetail({
    required this.productId,
    required this.productName,
    required this.code,
    required this.productDescription,
    required this.productImage,
    required this.isSold,
    required this.details,
    required this.images,
  });

  factory ProductDetail.fromJson(Map<String, dynamic> json) {
    return ProductDetail(
      productId: json['product_id'],
      productName: json['product_name'],
      code: json['code'],
      productDescription: json['product_description'],
      productImage: json['product_image'],
      isSold: json['is_sold'] == 1,
      details: List<Detail>.from(json['details'].map((x) => Detail.fromJson(x))),
      images: List<String>.from(json['images']),
    );
  }
}

class Detail {
  final int detailsId;
  final String color;
  final String hexCode;
  final String size;

  Detail({
    required this.detailsId,
    required this.color,
    required this.hexCode,
    required this.size,
  });

  factory Detail.fromJson(Map<String, dynamic> json) {
    return Detail(
      detailsId: json['details_id'],
      color: json['color'],
      hexCode: json['hex_code'],
      size: json['sizes'],
    );
  }
}
