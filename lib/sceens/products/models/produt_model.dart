// models/product_model.dart
import 'dart:convert';

ProductResponse productResponseFromJson(String str) => ProductResponse.fromJson(json.decode(str));

class ProductResponse {
  final List<Products> data;
  final Pagination pagination;

  ProductResponse({
    required this.data,
    required this.pagination,
  });

  factory ProductResponse.fromJson(Map<String, dynamic> json) {
    return ProductResponse(
      data: List<Products>.from(json['data'].map((x) => Products.fromJson(x))),
      pagination: Pagination.fromJson(json['pagination']),
    );
  }
}

class Products {
  final int id;
  final String name;
  final String description;
  final String image;
  final bool isSold;
  final List<ProductsDetail> details;

  Products({
    required this.id,
    required this.name,
    required this.description,
    required this.image,
    required this.isSold,
    required this.details,
  });

  factory Products.fromJson(Map<String, dynamic> json) {
    return Products(
      id: json['product_id'] ?? 0,
      name: json['product_name'] ?? '',
      description: json['product_description'] ?? '',
      image: json['product_image'] ?? '',
      isSold: json['is_sold'] == 1,
      details: json['details'] != null
          ? List<ProductsDetail>.from(json['details'].map((x) => ProductsDetail.fromJson(x)))
          : [],
    );
  }
}

class ProductsDetail {
  final String color;
  final String sizes;

  ProductsDetail({
    required this.color,
    required this.sizes,
  });

  factory ProductsDetail.fromJson(Map<String, dynamic> json) {
    return ProductsDetail(
      color: json['color'] ?? '',
      sizes: json['sizes'] ,
    );
  }
}

class Pagination {
  final int currentPage;
  final int perPage;
  final int total;
  final int lastPage;
  final String? nextPageUrl;
  final String? prevPageUrl;

  Pagination({
    required this.currentPage,
    required this.perPage,
    required this.total,
    required this.lastPage,
    this.nextPageUrl,
    this.prevPageUrl,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) {
    return Pagination(
      currentPage: json['current_page'] ?? 0,
      perPage: json['per_page'] ?? 0,
      total: json['total'] ?? 0,
      lastPage: json['last_page'] ?? 0,
      nextPageUrl: json['next_page_url'],
      prevPageUrl: json['prev_page_url'],
    );
  }
}
