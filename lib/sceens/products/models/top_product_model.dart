// To parse this JSON data, do
//
//     final topProduct = topProductFromJson(jsonString);

import 'dart:convert';

TopProduct topProductFromJson(String str) => TopProduct.fromJson(json.decode(str));

String topProductToJson(TopProduct data) => json.encode(data.toJson());

class TopProduct {
  List<TopProductDetails> data;

  TopProduct({
    required this.data,
  });

  factory TopProduct.fromJson(Map<String, dynamic> json) => TopProduct(
    data: List<TopProductDetails>.from(json["data"].map((x) => TopProductDetails.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class TopProductDetails {
  int id;
  String name;
  String description;
  String code;
  String basicImage;
  int isSold;
  int favoriatesCount;

  TopProductDetails({
    required this.id,
    required this.name,
    required this.description,
    required this.code,
    required this.basicImage,
    required this.isSold,
    required this.favoriatesCount,
  });

  factory TopProductDetails.fromJson(Map<String, dynamic> json) => TopProductDetails(
    id: json["id"],
    name: json["name"],
    description: json["description"],
    code: json["code"],
    basicImage: json["basic_image"],
    isSold: json["is_sold"],
    favoriatesCount: json["favoriates_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
    "code": code,
    "basic_image": basicImage,
    "is_sold": isSold,
    "favoriates_count": favoriatesCount,
  };
}
