import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/sceens/products/product_controller.dart';
import 'dart:typed_data';

import '../../const.dart';
import 'componet/image_picker.dart';

class CreateProductPage extends StatefulWidget {
  @override
  _CreateProductPageState createState() => _CreateProductPageState();
}

class _CreateProductPageState extends State<CreateProductPage> {
  final ProductController productController = Get.put(ProductController());
  final _formKey = GlobalKey<FormState>();
  final ImagePickerService imagePickerService = ImagePickerService();

  String name = '';
  String description = '';
  String code = '';
  String details = '';
  Uint8List? basicImage;
  List<Uint8List> additionalImages = [];

  List<int> selectedColors = [];
  List<int> selectedSizes = [];
  List<int> selectedShapes = [];
  List<int> selectedFabrics = [];
  List<int> selectedCategories = [];

  List<dynamic> colors = [];
  List<dynamic> sizes = [];
  List<dynamic> shapes = [];
  List<dynamic> fabrics = [];
  List<dynamic> categories = [];

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  Future<void> _fetchData() async {
    try {
      colors = await productController.getColors();
      sizes = await productController.getSizes();
      shapes = await productController.getShapes();
      fabrics = await productController.getFabrics();
      categories = await productController.getCategories();
    } catch (e) {
      print('Error fetching data: $e');
    } finally {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<void> _pickImage() async {
    final pickedFile = await imagePickerService.pickImage();
    if (pickedFile != null) {
      setState(() {
        basicImage = pickedFile;
      });
    }
  }

  Future<void> _pickAdditionalImages() async {
    final pickedFiles = await imagePickerService.pickMultipleImages();
    if (pickedFiles != null) {
      setState(() {
        additionalImages = pickedFiles;
      });
    }
  }
  final nameController = TextEditingController();
  final codeController = TextEditingController();
  final descriptionController = TextEditingController();
  final detailsController = TextEditingController();

  void _submitForm() {
    if (_formKey.currentState!.validate() && basicImage != null) {
      _formKey.currentState!.save(); // Save the form state

      Map<String, dynamic> productData = {
        'name': name,
        'description': description,
        'code': code,
        'details': details,
        'colors': selectedColors,
        'sizes': selectedSizes,
        'shapes': selectedShapes,
        'fabrics': selectedFabrics,
        'categories': selectedCategories,
      };

      productController.createProduct(
          productData, basicImage, additionalImages);

      Get.back(); // Close the form page after submission
    } else {
      // Show an error message if the form is not valid or image is not selected
      Get.snackbar('Error', 'Please fill all fields and select an image');
    }
  }

  Widget _buildCheckboxList(
      String title, List<dynamic> items, List<int> selectedItems) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title, style: TextStyle(fontWeight: FontWeight.bold,color: purple1,fontSize: 15)),
        Wrap(
          children: items.map((item) {
            return CheckboxListTile(
              title: Text(item['name']),
              value: selectedItems.contains(item['id']),
              onChanged: (bool? value) {
                setState(() {
                  if (value == true) {
                    selectedItems.add(item['id']);
                  } else {
                    selectedItems.remove(item['id']);
                  }
                });
              },
            );
          }).toList(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MainView(
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Container(
          decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 3),
            ),
          ],
        ),
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.offNamed('/ProductsPage');
                },
              ),              title: Text(
                'Create Product',
                style: TextStyle(fontWeight: FontWeight.bold,color: purple1),
              ),
            ),

            body: Padding(
              padding: const EdgeInsets.all(16.0),
              child: isLoading
                  ? Center(child: CircularProgressIndicator())
                  : Form(
                      key: _formKey,
                      child: ListView(
                        children: [  SizedBox(
                          height: defaultPadding,
                        ),
                          TextFormField(
                           controller:nameController,

                            decoration: InputDecoration(
                                labelText: 'Name', border: OutlineInputBorder()),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a name';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              name = value!;
                            },
                          ),
                          SizedBox(
                            height: defaultPadding,
                          ),
                          TextFormField(
                            //controller: descriptionController,
                            decoration: InputDecoration(
                                labelText: 'Description',
                                border: OutlineInputBorder()),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a description';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              description = value!;
                            },
                          ),
                          SizedBox(
                            height: defaultPadding,
                          ),
                          TextFormField(
                            //controller: codeController,
                            decoration: InputDecoration(
                                labelText: 'Code', border: OutlineInputBorder()),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a code';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              code = value!;
                            },
                          ),
                          SizedBox(
                            height: defaultPadding,
                          ),
                          TextFormField(
                            //controller: detailsController,
                            decoration: InputDecoration(
                                labelText: 'Details', border: OutlineInputBorder()),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter details';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              details = value!;
                            },
                          ),
                          SizedBox(
                            height: defaultPadding,
                          ),
                          _buildCheckboxList('Colors', colors, selectedColors),                      SizedBox(
                            height: defaultPadding,
                          ),

                          _buildCheckboxList('Sizes', sizes, selectedSizes),                      SizedBox(
                            height: defaultPadding,
                          ),

                          _buildCheckboxList('Shapes', shapes, selectedShapes),                      SizedBox(
                            height: defaultPadding,
                          ),

                          _buildCheckboxList('Fabrics', fabrics, selectedFabrics),                      SizedBox(
                            height: defaultPadding,
                          ),

                          _buildCheckboxList(
                              'Categories', categories, selectedCategories),
                          SizedBox(height: 16.0),
                          Row(
                            children: [
                              ElevatedButton(
                                onPressed: _pickImage,
                                child: Text('Pick Basic Image'),
                              ),
                              SizedBox(width: 16.0),
                              basicImage != null
                                  ? Image.memory(basicImage!, width: 50, height: 50)
                                  : Container(),
                            ],
                          ),
                          SizedBox(height: 16.0),
                          ElevatedButton(
                            onPressed: _pickAdditionalImages,
                            child: Text('Pick Additional Images'),
                          ),
                          Wrap(
                            children: additionalImages.map((image) {
                              return Image.memory(image, width: 50, height: 50);
                            }).toList(),
                          ),
                          SizedBox(height: 16.0),
                          ElevatedButton(
                            onPressed: _submitForm,
                            child: Text('Submit'),
                          ),
                        ],
                      ),
                    ),
            ),
          ),
        ),
      ),
      );

  }
}
