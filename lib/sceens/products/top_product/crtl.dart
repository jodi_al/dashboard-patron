import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/products/top_product/services.dart';
import '../models/top_product_model.dart';

class TopProductController extends GetxController {
  final TopProductService topProductService = Get.put(TopProductService());

  var topProducts = <TopProductDetails>[].obs;
  var isLoading = false.obs;
  var errorMessage = ''.obs;

  @override
  void onInit() {
    fetchTopProducts();
    super.onInit();
  }

  void fetchTopProducts() async {
    isLoading(true);
    errorMessage('');
    try {
      TopProduct response = await topProductService.getTopProducts();
      topProducts.value = response.data;
      print('Top products fetched: ${topProducts.length}');
    } catch (e) {
      errorMessage(e.toString());
      print('Error fetching top products: $e');
    } finally {
      isLoading(false);
    }
  }
}
