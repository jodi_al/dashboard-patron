import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import '../../../confing/confing.dart';
import '../models/top_product_model.dart';

class TopProductService extends GetxService {
  var isLoading = true.obs;

  Future<TopProduct> getTopProducts() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }

    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.topProduct),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      final jsonResponse = json.decode(response.body);
      print('Parsed JSON Response: $jsonResponse');
      return TopProduct.fromJson(jsonResponse);
    } else {
      throw Exception('Failed to load top products');
    }
  }
}
