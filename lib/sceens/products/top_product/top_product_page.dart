import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/sceens/products/top_product/crtl.dart';

import '../../../responseive.dart';

class TopProductsPage extends StatelessWidget {
  final TopProductController topProductController =
  Get.put(TopProductController());

  @override
  Widget build(BuildContext context) {
    return Responsive(
      // Define different layouts for mobile, tablet, and desktop
      mobile: buildGridView(2), // 2 items per row for mobile
      tablet: buildGridView(4), // 4 items per row for tablet
      desktop: buildGridView(5), // 5 items per row for desktop
    );
  }

  Widget buildGridView(int crossAxisCount) {
    return MainView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.offNamed('/ProductsPage');
                },
              ),
              title: Text(
                'Top Products',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            body: Obx(() {
              if (topProductController.isLoading.value) {
                return Center(child: CircularProgressIndicator());
              } else if (topProductController.errorMessage.isNotEmpty) {
                return Center(
                    child: Text(topProductController.errorMessage.value));
              } else if (topProductController.topProducts.isEmpty) {
                return Center(child: Text('No top products available'));
              } else {
                return GridView.builder(
                  padding: const EdgeInsets.all(8.0),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: crossAxisCount,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 0.8,
                  ),
                  itemCount: topProductController.topProducts.length,
                  itemBuilder: (context, index) {
                    var product = topProductController.topProducts[index];
                    return Card(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(
                            child: Image.network(
                              product.basicImage,
                              fit: BoxFit.cover,
                              errorBuilder: (context, error, stackTrace) {
                                return Icon(Icons.image_not_supported,
                                    size: 50);
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  product.name,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: 5),
                                Text(
                                  product.description,
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                );
              }
            }),
          ),
        ),
      ),
    );
  }
}
