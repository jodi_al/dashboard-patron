import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/products/details_page/services.dart';
import 'package:patron_dashboard/sceens/products/models/details_model.dart';

class ProductDetailsController extends GetxController {
  final ProductDetailsService _productService = ProductDetailsService();
  var isLoading = true.obs;
  var productDetails = Rxn<ProductDetail>();

  var errorMessage = ''.obs;


  void fetchProductDetails(int id) async {
    isLoading(true);
    try {

      print(121212);

      ProductDetail response = await _productService.getProductDetails(id);
      productDetails.value = response;
      print("productDetails is");
      print(productDetails);// it comes null
      print(response);
      errorMessage('');
    } catch (e) {
      errorMessage(e.toString());
    } finally {
      isLoading(false);
    }
  }

  void deleteProduct(int id) async {
    isLoading(true);
    try {
      await _productService.deleteProduct(id);
      Get.back();  // Navigate back to the previous screen
      Get.snackbar('Success', 'Product deleted successfully',
          snackPosition: SnackPosition.BOTTOM);
    } catch (e) {
      Get.snackbar('Error', 'Failed to delete product: ${e.toString()}',
          snackPosition: SnackPosition.BOTTOM);
    } finally {
      isLoading(false);
    }
  }
}
