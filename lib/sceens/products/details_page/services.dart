import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:patron_dashboard/confing/confing.dart';
import 'package:patron_dashboard/sceens/products/models/details_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductDetailsService extends GetxService {
  Future<ProductDetail> getProductDetails(int id) async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }
    final response = await http.get(
      Uri.parse(
          '${ServerConfig.domainNameServer + ServerConfig.showProductDetailes}$id'),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print(response.body);
    print('00');

    if (response.statusCode == 200) {
      return ProductDetail.fromJson(json.decode(response.body)['data']);
    } else {
      throw Exception('Failed to load product details');
    }
  }

  Future<void> deleteProduct(int id) async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }

    final response = await http.delete(
      Uri.parse('${ServerConfig.domainNameServer + ServerConfig.deleteProduct}$id'),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode != 200) {
      throw Exception('Failed to delete product');
    }
  }
}
