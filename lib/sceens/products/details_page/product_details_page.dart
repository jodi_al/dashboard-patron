import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/confing/confing.dart';
import 'package:patron_dashboard/const.dart';
import '../models/details_model.dart';
import 'controller.dart';
import 'services.dart';

class ProductDetailsPage extends StatefulWidget {
  final int productId;
  ProductDetailsPage({Key? key, required this.productId}) : super(key: key);

  @override
  State<ProductDetailsPage> createState() => _ProductDetailsPageState();
}

class _ProductDetailsPageState extends State<ProductDetailsPage> {
  final ProductDetailsController productDetailsController =
      Get.put(ProductDetailsController());

  @override
  void initState() {
    super.initState();
    productDetailsController.fetchProductDetails(widget.productId);
  }


  @override
  Widget build(BuildContext context) {
    return MainView(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Product Details'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.offNamed('/ProductsPage');
            },
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                final confirm = await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('Delete Product'),
                      content:
                          Text('Are you sure you want to delete this product?'),
                      actions: [
                        TextButton(
                          onPressed: () => Navigator.of(context).pop(false),
                          child: Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.of(context).pop(true),
                          child: Text('Delete'),
                        ),
                      ],
                    );
                  },
                );

                if (confirm == true) {
                  productDetailsController.deleteProduct(widget.productId);
                }
              },
            ),
          ],
        ),
        body: Obx(() {
          if (productDetailsController.isLoading.value) {
            return Center(child: CircularProgressIndicator());
          } else if (productDetailsController.errorMessage.isNotEmpty) {
            return Center(
                child: Text(
                    'Error: ${productDetailsController.errorMessage.value}'));
          } else if (productDetailsController.productDetails.value == null) {
            return Center(child: Text('No data available'));
          } else {
            var productDetail = productDetailsController.productDetails.value;

            return SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center ,
                children: [
                  Text(
                    'Product Name: ${productDetail?.productName}',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),
                  Text(
                      'Product Description: ${productDetail?.productDescription}'),
                  SizedBox(height: 20),
                  Image.network(
                    '${ServerConfig.domainNameServer}${productDetail?.productImage}',
                    errorBuilder: (context, error, stackTrace) {
                      return Icon(Icons.image_not_supported, size: 50);
                    },
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Details',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: productDetail!.details.length,
                    itemBuilder: (context, index) {
                      var detail = productDetail!.details[index];
                      return ListTile(
                        title: Text('Color: ${detail!.color}'),
                        subtitle: Text('Size: ${detail!.size}'),
                        leading: Image.network(
                          '${ServerConfig.domainNameServer}${productDetail!.images[index]}',
                          errorBuilder: (context, error, stackTrace) {
                            return Icon(Icons.image_not_supported, size: 50);
                          },
                        ),
                      );
                    },
                  ),
                ],
              ),
            );
          }
        }),
      ),
    );
  }

  @override
  void dispose() {
    productDetailsController
        .dispose(); // Dispose any controllers or subscriptions
    super.dispose();
  }
}
