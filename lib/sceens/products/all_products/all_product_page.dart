import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/confing/confing.dart';
import '../../../const.dart';
import '../../../responseive.dart';
import '../details_page/product_details_page.dart';
import '../top_product/top_product_page.dart';
import 'crtl.dart';

class AllProductsPage extends StatelessWidget {
  final AllProductController productController = Get.put(AllProductController());

  @override
  Widget build(BuildContext context) {
    return Responsive(
      // Define different layouts for mobile, tablet, and desktop
      mobile: buildGridView(2), // 2 items per row for mobile
      tablet: buildGridView(3), // 3 items per row for tablet
      desktop: buildGridView(4), // 4 items per row for desktop
    );
  }

  Widget buildGridView(int crossAxisCount) {
    return MainView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text(
                'All Products',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              actions: [
                TextButton(
                  child: Text('Add Product', style: TextStyle(color: purple)),
                  onPressed: () {
                    Get.toNamed('/ProductFormPage');
                  },
                ),
                IconButton(
                  icon: Icon(Icons.star),
                  onPressed: () {
                    Get.to(() => TopProductsPage());
                  },
                ),
              ],
            ),
            body: Obx(() {
              if (productController.isLoading.value) {
                return Center(child: CircularProgressIndicator());
              } else if (productController.errorMessage.isNotEmpty) {
                return Center(child: Text(productController.errorMessage.value));
              } else if (productController.products.isEmpty) {
                return Center(child: Text('No products available'));
              } else {
                return GridView.builder(
                  padding: const EdgeInsets.all(8.0),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 400,
                    crossAxisSpacing: 30,
                    mainAxisSpacing: 10,
                    mainAxisExtent: 400,
                  ),
                  itemCount: productController.products.length,
                  itemBuilder: (context, index) {
                    var product = productController.products[index];
                    return Card(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              height: 300,
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: Image.network(
                                '${ServerConfig.domainNameServer}/${product.image}',
                                loadingBuilder: (BuildContext context, Widget child,
                                    ImageChunkEvent? loadingProgress) {
                                  if (loadingProgress == null) return child;
                                  return Center(
                                    child: CircularProgressIndicator(
                                      value: loadingProgress.expectedTotalBytes != null
                                          ? loadingProgress.cumulativeBytesLoaded /
                                              (loadingProgress.expectedTotalBytes ?? 1)
                                          : null,
                                    ),
                                  );
                                },
                                fit: BoxFit.cover,
                                errorBuilder: (context, error, stackTrace) {
                                  return Icon(Icons.image_not_supported, size: 50);
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Text(
                                      product.name,
                                      style: TextStyle(
                                          fontSize: 16, fontWeight: FontWeight.bold),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.edit_square, color: Color(0xff936b9f)),
                                    onPressed: () {
                                      // Handle edit action
                                      // Navigate to the product details page
                                      Get.to(() => ProductDetailsPage(productId: product.id));
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              }
            }),
            bottomNavigationBar: buildBottomNavigationBar(),
          ),
        ),
      ),
    );
  }

  Widget buildBottomNavigationBar() {
    return Padding(
      padding: const EdgeInsets.all(defaultPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: productController.loadPreviousPage,
            child: Text('Previous'),
          ),
          Obx(() => Text(
                'Page ${productController.pagination.value?.currentPage} of ${productController.pagination.value?.lastPage}',
              )),
          TextButton(
            onPressed: productController.loadNextPage,
            child: Text('Next'),
          ),
        ],
      ),
    );
  }
}
