// controllers/all_product_controller.dart
import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/products/all_products/services.dart';
import '../models/produt_model.dart';

class AllProductController extends GetxController {
  final AllProductService productService = Get.put(AllProductService());
  var selectedProduct = Rxn<Products>();

  var products = <Products>[].obs;
  var pagination = Rxn<Pagination>();
  var isLoading = false.obs;
  var errorMessage = ''.obs;

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }
  void selectProduct(Products product) {
    selectedProduct.value = product;
  }

  void fetchProducts({int page = 1}) async {
    isLoading(true);
    errorMessage('');
    try {
      ProductResponse response = await productService.getAllProducts(page: page);
      products.value = response.data;
      pagination.value = response.pagination;
    } catch (e) {
      errorMessage(e.toString());
    } finally {
      isLoading(false);
    }
  }

  void loadNextPage() {
    if (pagination.value?.nextPageUrl != null) {
      fetchProducts(page: pagination.value!.currentPage + 1);
    }
  }

  void loadPreviousPage() {
    if (pagination.value?.prevPageUrl != null) {
      fetchProducts(page: pagination.value!.currentPage - 1);
    }
  }
}
