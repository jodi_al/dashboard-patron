import 'dart:typed_data';

import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/products/product_services.dart';
import 'models/produt_model.dart';
import 'dart:io';

class ProductController extends GetxController {
  var isLoading = false.obs;
  var productList = <Products>[].obs;
  ProductService productService = ProductService();

  @override
  void onInit() {
    super.onInit();
  }


  void createProduct(Map<String, dynamic> productData, dynamic basicImage, List<dynamic> additionalImages) async {
    try {
      isLoading(true);
      var product = await productService.createProduct(productData, basicImage, additionalImages);
      if (product != null) {
        productList.add(product);
        print("Product created: ${product.name}");
      }
    } finally {
      isLoading(false);
    }
  }

  Future<List<dynamic>> getColors() async {
    return await productService.getColors();
  }

  Future<List<dynamic>> getSizes() async {
    return await productService.getSizes();
  }

  Future<List<dynamic>> getShapes() async {
    return await productService.getShapes();
  }

  Future<List<dynamic>> getCategories() async {
    return await productService.getCategories();
  }

  Future<List<dynamic>> getFabrics() async {
    return await productService.getFabrics();
  }


}
