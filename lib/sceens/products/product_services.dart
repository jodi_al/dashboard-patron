// services/product_service.dart
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:patron_dashboard/confing/confing.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../confing/set_header.dart';
import 'models/produt_model.dart';

class ProductService extends GetxService {
  var isLoading = true.obs;

// Future<Map<String, dynamic>> createProduct(
  //     {
  //   required String name,
  //   required String description,
  //   required String code,
  //   required File basicImage,
  //   required int colorId,
  //   required int sizeId,
  //   required int shapeId,
  //   required int fabricId,
  //   required int categoryId,
  //   required File additionalImage,
  // }) async
  // {
  //   var request = http.MultipartRequest('POST', Uri.parse(ServerConfig.domainNameServer+ServerConfig.createProduct));
  //   request.fields['name'] = name;
  //   request.fields['description'] = description;
  //   request.fields['code'] = code;
  //   request.fields['details[0][color_id]'] = colorId.toString();
  //   request.fields['details[0][size_id]'] = sizeId.toString();
  //   request.fields['shapes[0]'] = shapeId.toString();
  //   request.fields['fabrics[0]'] = fabricId.toString();
  //   request.fields['categories[0]'] = categoryId.toString();
  //
  //   request.files.add(await http.MultipartFile.fromPath(
  //     'basic_image',
  //     basicImage.path,
  //     contentType: MediaType('image', 'jpeg'),
  //   ));
  //
  //   request.files.add(await http.MultipartFile.fromPath(
  //     'images[0]',
  //     additionalImage.path,
  //     contentType: MediaType('image', 'jpeg'),
  //   ));
  //
  //   var response = await request.send();
  //
  //   if (response.statusCode == 201) {
  //     var responseData = await response.stream.bytesToString();
  //     return json.decode(responseData);
  //   } else {
  //     throw Exception('Failed to create product');
  //   }
  // }
  Future<ProductResponse> getTopProducts({int page = 1}) async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }

    final response = await http.get(
      Uri.parse('${ServerConfig.domainNameServer + ServerConfig.topProduct}?page=$page'),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ProductResponse.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load top products');
    }
  }

  Future<List<dynamic>> getColors() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      throw Exception('Token not found. Please login again.');
    }
    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getColors),
      headers: {
        'Authorization': 'Bearer $token'
      },
    );
    print(response.statusCode);
    print('colors');

    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    } else {
      throw Exception('Failed to load colors');
    }
  }

  Future<List<dynamic>> getSizes() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      throw Exception('Token not found. Please login again.');
    }
    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getSizes),
      headers: {
        'Authorization': 'Bearer $token'
      },
    );
    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    } else {
      throw Exception('Failed to load sizes');
    }
  }

  Future<List<dynamic>> getShapes() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      throw Exception('Token not found. Please login again.');
    }
    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getShapes),
      headers: {
        'Authorization': 'Bearer $token'
      },
    );
    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    } else {
      throw Exception('Failed to load shapes');
    }
  }

  Future<List<dynamic>> getCategories() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      throw Exception('Token not found. Please login again.');
    }
    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getAllCategories),
      headers: {
        'Authorization': 'Bearer $token'
      },
    );
    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    } else {
      throw Exception('Failed to load categories');
    }
  }

  Future<List<dynamic>> getFabrics() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      throw Exception('Token not found. Please login again.');
    }
    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getAllFabrics),
      headers: {
        'Authorization': 'Bearer $token'
      },
    );
    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    } else {
      throw Exception('Failed to load fabrics');
    }
  }

  Future<Products?> createProduct(Map<String, dynamic> productData,
      dynamic basicImage, List<dynamic> additionalImages) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      String? token = sharedPreferences.getString('data');
      if (token == null) {
        throw Exception('Token not found. Please login again.');
      }
      var request = http.MultipartRequest(
        'POST',
        Uri.parse(ServerConfig.domainNameServer + ServerConfig.createProduct),
      );
      var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      };
      request.headers.addAll(headers);

      productData.forEach((key, value) {
        if (value is List) {
          for (var item in value) {
            request.fields['$key[]'] = item.toString();
          }
        } else {
          request.fields[key] = value.toString();
        }
      });

      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
            'basic_image', basicImage, filename: 'basic_image.png'));
        for (var image in additionalImages) {
          request.files.add(http.MultipartFile.fromBytes(
              'images[]', image, filename: 'additional_image.png'));
        }
      } else {
        request.files.add(await http.MultipartFile.fromPath(
            'basic_image', (basicImage as File).path));
        for (var image in additionalImages) {
          request.files.add(await http.MultipartFile.fromPath(
              'images[]', (image as File).path));
        }
      }

      print("Sending request with fields: ${request.fields}");
      print("Sending request with files: ${request.files.map((f) => f.filename)
          .toList()}");

      var response = await request.send();
      if (response.statusCode == 200) {
        var responseBody = await response.stream.bytesToString();
        var jsonResponse = json.decode(responseBody);
        return Products.fromJson(jsonResponse['data']);
      } else {
        print('Failed to create product: ${response.statusCode}');
        print('Response body: ${await response.stream.bytesToString()}');
        return null;
      }
    } catch (e) {
      print('Error creating product: $e');
      return null;
    }
  }

}
