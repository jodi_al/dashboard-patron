import 'dart:typed_data';
import 'package:file_picker/file_picker.dart';

class ImagePickerService {
  Future<Uint8List?> pickImage() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.image,
    );
    if (result != null && result.files.single.bytes != null) {
      return result.files.single.bytes;
    }
    return null;
  }

  Future<List<Uint8List>?> pickMultipleImages() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.image,
      allowMultiple: true,
    );
    if (result != null) {
      return result.files.map((file) => file.bytes!).toList();
    }
    return null;
  }
}
