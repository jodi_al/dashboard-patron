class Message {
  final String? sender;
  final int? senderId;
  final String? content;
  final String? content2;
  final DateTime? timestamp;

  Message(
      {this.sender,
      this.senderId,
      this.content,
      this.content2,
      this.timestamp});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      sender: json['sender_name'],
      senderId: json['sender_id'],
      content: json['message'],
      content2: json['massage'],
      timestamp: json['created_at'] != null
          ? DateTime.parse(json['created_at'])
          : null,
    );
  }
}
