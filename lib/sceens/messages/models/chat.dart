import 'package:patron_dashboard/sceens/customers/models/customer.dart';
import 'package:patron_dashboard/sceens/messages/models/message.dart';

class Chat {
  final int id;
  final CustomerData user;
  final Message? message;
  Chat({required this.id, required this.user, this.message});

  factory Chat.fromJson(Map<String, dynamic> json) {
    return Chat(
        id: json['id'],
        user: CustomerData.fromJson(json['user']),
        message: Message.fromJson(json['latest_message']));
  }
}
