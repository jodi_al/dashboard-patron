import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'controllers/chat_controller.dart';

class MassagePage extends StatefulWidget {
  @override
  State<MassagePage> createState() => _MassagePageState();
}

class _MassagePageState extends State<MassagePage> {
  final ChatController chatController = Get.put(ChatController());

  @override
  void initState() {
    chatController.fetchChats();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var textEditingController = TextEditingController();

    return MainView(
      child: Scaffold(
        body: Row(
          children: [
            // Left Side (Conversations List)
            Container(
              width: 300,
              color: Colors.white,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Conversations',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        CircleAvatar(
                          radius: 15,
                          child:
                              Obx(() => Text('${chatController.chats.length}')),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Search...',
                        fillColor: Color(0xffE2E8F0),
                        filled: true,
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Obx(() {
                      if (chatController.chats.isEmpty) {
                        return Center(child: CircularProgressIndicator());
                      } else {
                        return ListView.builder(
                          itemCount: chatController.chats.length,
                          padding: EdgeInsets.all(16),
                          itemBuilder: (context, index) {
                            final chat = chatController.chats[index];
                            return Container(
                              decoration: BoxDecoration(
                                  color: chatController.lastChatId != null &&
                                          chatController.lastChatId! == chat.id
                                      ? Color(0xffE2E8F0)
                                      : null,
                                  borderRadius: BorderRadius.circular(10)),
                              child: ListTile(
                                leading: CircleAvatar(
                                  child: Icon(Icons.person),
                                ),
                                title: Text(chat.user.fullName),
                                subtitle: Text(chat.message?.content2 ?? ''),
                                onTap: () {
                                  chatController.fetchMessages(chat.id);
                                  setState(() {});
                                },
                              ),
                            );
                          },
                        );
                      }
                    }),
                  ),
                ],
              ),
            ),
            // Right Side (Chat Area)
            VerticalDivider(),
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 20,
                          child: Icon(Icons.person),
                        ),
                        SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Chat with User',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  // Messages List
                  Expanded(
                    child: Obx(() {
                      if (chatController.messages.isEmpty) {
                        return Center(child: CircularProgressIndicator());
                      } else {
                        return ListView.builder(
                          itemCount: chatController.messages.length,
                          itemBuilder: (context, index) {
                            final message = chatController.messages[index];
                            bool isCurrentUser = message.senderId.toString() ==
                                chatController.currentUser;

                            return Align(
                              alignment: isCurrentUser
                                  ? Alignment.centerRight
                                  : Alignment.centerLeft,
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: isCurrentUser
                                      ? Color(0xff936B9F)
                                      : Color(0xffE2E8F0),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      message.sender ?? '',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: isCurrentUser
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      message.content ?? '',
                                      style: TextStyle(
                                        color: isCurrentUser
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    if (message.timestamp != null)
                                      Text(
                                        message.timestamp!.toLocal().toString(),
                                        style: TextStyle(
                                          color: isCurrentUser
                                              ? Colors.white70
                                              : Colors.black54,
                                          fontSize: 12,
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      }
                    }),
                  ),
                  Divider(),
                  // Message Input
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: textEditingController,
                            decoration: InputDecoration(
                              hintText: 'Type a message...',
                              fillColor: Color(0xffE2E8F0),
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide.none,
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.send),
                          onPressed: () {
                            chatController.sendMessage(
                              textEditingController.text,
                            );
                            textEditingController.clear();
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
