import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:patron_dashboard/sceens/messages/models/chat.dart';
import 'package:patron_dashboard/sceens/messages/models/message.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import '../../../confing/confing.dart';

class ChatController extends GetxController {
  var messages = <Message>[].obs;
  var chats = <Chat>[].obs; // Add this line to store chats
  String? currentUser = '1';
  late final String? token;
  int? lastChatId;

  @override
  void onInit() async {
    super.onInit();

    // fetchMessages();
    fetchChats(); // Fetch chats when the controller is initialized
  }

  Future<void> loadToken() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    token = sharedPreferences.getString('data');
    print(token);
    // if (token == null) {
    //   Get.snackbar(
    //     'Error',
    //     'Token not found. Please login again.',
    //     snackPosition: SnackPosition.BOTTOM,
    //   );
    //   throw Exception('Token not found');
    // }
  }

  Future<void> fetchMessages(int chatId) async {
    // try {
    lastChatId = chatId;
    final response = await http.get(
        Uri.parse('${ServerConfig.domainNameServer}/api/showchat/$chatId'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body);
      messages.value = data.map((json) => Message.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load messages');
    }
    // } catch (e) {
    //   print("Error fetching messages: $e");
    // }
  }

  Future<void> fetchChats() async {
    
      // if(token == null){
      await loadToken();
      // }
      final response = await http.get(
          Uri.parse('${ServerConfig.domainNameServer}/api/chats'),
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          });
      if (response.statusCode == 200) {
        final Map<String, dynamic> resData = json.decode(response.body);
        if (resData['success']) {
          final List<dynamic> data = resData['data'];
          chats.value = data.map((json) => Chat.fromJson(json)).toList();
        } else {
          throw Exception('Failed to load chats');
        }
      } else {
        throw Exception('Failed to load chats');
      }

  }

  Future<void> sendMessage(String message) async {
    try {
      final response = await http.post(
        Uri.parse(
            '${ServerConfig.domainNameServer}/api/sendMessage/$lastChatId'),
      headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          },
        body: 
          {
          'message': message,
        }
        ,
      );

      if (response.statusCode == 200) {
        if (lastChatId != null) {
          fetchMessages(lastChatId!);
        }
      } else {
        throw Exception('Failed to send message');
      }
    } catch (e) {
      print("Error sending message: $e");
    }
  }
}
