import 'package:flutter/material.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/sceens/reports/controllers/report_controller.dart';

class ReportsPage extends StatefulWidget {
  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportsPage> {
  DateTime _fromMonth = DateTime.now();
  DateTime _toMonth = DateTime.now();
  late ReportController _reportController;

  @override
  void initState() {
    super.initState();
    _reportController = ReportController(_fromMonth, _toMonth);
  }

  void _searchOrders() {
    setState(() {
      _reportController.fetchOrders();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MainView(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Reports'),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildFilterSection(),
              SizedBox(height: 16),
              _buildSummarySection(),
              SizedBox(height: 16),
              _buildDetailsSection(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFilterSection() {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: _buildMonthPicker('From Month', _fromMonth,
                      (date) => setState(() => _fromMonth = date)),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: _buildMonthPicker('To Month', _toMonth,
                      (date) => setState(() => _toMonth = date)),
                ),
              ],
            ),
            SizedBox(height: 8),
            ElevatedButton(
              onPressed: _searchOrders,
              child: Text('Search'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMonthPicker(
      String label, DateTime date, ValueChanged<DateTime> onDateChanged) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(label),
        SizedBox(height: 4),
        GestureDetector(
          onTap: () async {
            DateTime? picked = await showDatePicker(
              context: context,
              initialDate: date,
              firstDate: DateTime(2000),
              lastDate: DateTime(2101),
              initialDatePickerMode: DatePickerMode.year,
            );
            if (picked != null && picked != date) onDateChanged(picked);
          },
          child: AbsorbPointer(
            child: TextFormField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
              ),
              initialValue: "${date.toLocal()}".split(' ')[0].substring(0, 7),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSummarySection() {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Summary',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
            SizedBox(height: 8),
            _buildTotalCard(
              'Best Order Month',
              _reportController.summaryDetails['Best Order Month'] ?? 'N/A',
              Icons.calendar_today,
              Colors.blue,
              {},
            ),
            _buildTotalCard(
                'Most Ordered Product',
                _reportController.summaryDetails['Most Ordered Product'] ??
                    'N/A',
                Icons.shopping_cart,
                Colors.green,
                {'Orders': '120'}),
            _buildTotalCard(
                'Most Ordered Color',
                _reportController.summaryDetails['Most Ordered Color'] ?? 'N/A',
                Icons.color_lens,
                Colors.red,
                {'Orders': '85'}),
            _buildTotalCard(
                'Most Ordered Size',
                _reportController.summaryDetails['Most Ordered Size'] ?? 'N/A',
                Icons.straighten,
                Colors.orange,
                {'Orders': '100'}),
            _buildTotalCard(
                'Most Ordered Fabric',
                _reportController.summaryDetails['Most Ordered Fabric'] ??
                    'N/A',
                Icons.texture,
                Colors.purple,
                {'Orders': '95'}),
            _buildTotalCard(
                'Most Ordered Shape',
                _reportController.summaryDetails['Most Ordered Shape'] ?? 'N/A',
                Icons.crop_square,
                Colors.brown,
                {'Orders': '90'}),
            _buildTotalCard(
                'Working Hours',
                _reportController.summaryDetails['Working Hours'] ?? 'N/A',
                Icons.access_time,
                Colors.teal,
                {'Hours': '160'}),
          ],
        ),
      ),
    );
  }

  Widget _buildTotalCard(String title, String total, IconData icon,
      Color iconColor, Map<String, String> details) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          children: <Widget>[
            Icon(icon, size: 40, color: iconColor),
            SizedBox(width: 16),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(title,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  SizedBox(height: 8),
                  Text('TOTAL: $total', style: TextStyle(fontSize: 24)),
                  Divider(),
                  ...details.entries
                      .map((entry) => Text('${entry.key}: ${entry.value}')),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDetailsSection() {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Order Details',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
            SizedBox(height: 8),
            _buildDataTable(),
          ],
        ),
      ),
    );
  }

  Widget _buildDataTable() {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Table(
          border: TableBorder.all(color: Colors.grey),
          columnWidths: {
            0: FlexColumnWidth(2),
            1: FlexColumnWidth(2),
            2: FlexColumnWidth(2),
            3: FlexColumnWidth(2),
            4: FlexColumnWidth(2),
            5: FlexColumnWidth(2),
          },
          children: [
            TableRow(
              decoration: BoxDecoration(color: Colors.grey[200]),
              children: [
                _buildTableCell('Product'),
                _buildTableCell('Color'),
                _buildTableCell('Size'),
                _buildTableCell('Fabric'),
                _buildTableCell('Shape'),
                _buildTableCell('Order Count'),
              ],
            ),
            ..._reportController.orderDetails.map((order) {
              return TableRow(
                children: [
                  _buildTableCell(order['product']),
                  _buildTableCell(order['color']),
                  _buildTableCell(order['size']),
                  _buildTableCell(order['fabric']),
                  _buildTableCell(order['shape']),
                  _buildTableCell(order['orderCount']),
                ],
              );
            }).toList(),
          ],
        ),
      ),
    );
  }

  Widget _buildTableCell(String text) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(text, textAlign: TextAlign.center),
    );
  }
}
