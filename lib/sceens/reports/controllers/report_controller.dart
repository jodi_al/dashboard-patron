// report_controller.dart
import 'package:flutter/material.dart';

class ReportController {
  DateTime fromDate;
  DateTime toDate;
  List<Map<String, dynamic>> orderDetails = [];
  Map<String, String> summaryDetails = {
    'Best Order Month': 'N/A',
    'Most Ordered Product': 'N/A',
    'Most Ordered Color': 'N/A',
    'Most Ordered Size': 'N/A',
    'Most Ordered Fabric': 'N/A',
    'Most Ordered Shape': 'N/A'
  };

  ReportController(this.fromDate, this.toDate);

  void fetchOrders() {
    // Simulate fetching data from a backend or data source
    orderDetails = [
      {
        'product': 'Product A',
        'color': 'Red',
        'size': 'M',
        'fabric': 'Cotton',
        'shape': 'Round',
        'orderCount': '120',
      },
      {
        'product': 'Product B',
        'color': 'Blue',
        'size': 'L',
        'fabric': 'Silk',
        'shape': 'Square',
        'orderCount': '95',
      },
      // Add more order details as needed
    ];

    summaryDetails = {
      'Best Order Month': 'June',
      'Most Ordered Product': 'Product A',
      'Most Ordered Color': 'Red',
      'Most Ordered Size': 'M',
      'Most Ordered Fabric': 'Cotton',
      'Most Ordered Shape': 'Round'
    };
  }
}
