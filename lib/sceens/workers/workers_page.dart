import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/workers/worker_ctrl.dart';
import '../../componets/Main_view.dart';
import 'components/worker_card.dart'; // Import the WorkerCard

class WorkersPage extends StatelessWidget {
  final WorkerController workerController = Get.put(WorkerController());

  @override
  Widget build(BuildContext context) {
    return MainView(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Workers',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  Obx(() => ElevatedButton(
                    onPressed: () {
                      if (workerController.showingTopWorkers.value) {
                        workerController.fetchWorkers();
                      } else {
                        workerController.fetchTopWorkers();
                      }
                    },
                    child: Text(workerController.showingTopWorkers.value
                        ? 'Show All Workers'
                        : 'Show Top Workers'),
                  )),
                ],
              ),
              SizedBox(height: 20),
              Obx(() {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    WorkerCard(
                      title: 'Total Workers',
                      value: workerController.totalWorkersCount.value.toString(),
                      icon: Icons.people,
                    ),
                    if (workerController.showingTopWorkers.value)
                      WorkerCard(
                        title: 'Top Workers',
                        value: workerController.topWorkersCount.value.toString(),
                        icon: Icons.star,
                      ),
                  ],
                );
              }),
              SizedBox(height: 20),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(() {
                        if (workerController.isLoading.value) {
                          return Center(child: CircularProgressIndicator());
                        } else if (workerController.errorMessage.isNotEmpty) {
                          return Center(child: Text(workerController.errorMessage.value));
                        } else {
                          return SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: DataTable(
                              columns: const [
                                DataColumn(
                                  label: Text(
                                    'Worker Name',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    'Phone Number',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    'Email',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    'Address',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    'Is store',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    'Details',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ],
                              rows: workerController.showingTopWorkers.value
                                  ? workerController.topWorkers.map((worker) {
                                return DataRow(
                                  cells: [
                                    DataCell(
                                      Text(worker.fullName),
                                    ),
                                    DataCell(Text(worker.phoneNumber)),
                                    DataCell(Text(worker.email)),
                                    DataCell(Text(worker.address ?? 'N/A')),
                                    DataCell(Text(worker.isStore == 1 ? 'Store' : 'Individual')),
                                    DataCell(
                                      TextButton(
                                        onPressed: () {
                                          Get.toNamed('/WorkerDetailsPage', arguments: worker.id);
                                        },
                                        child: Text('More'),
                                      ),
                                    ),
                                  ],
                                );
                              }).toList()
                                  : workerController.workers.map((worker) {
                                return DataRow(
                                  cells: [
                                    DataCell(
                                      Text(worker.fullName),
                                    ),
                                    DataCell(Text(worker.phoneNumber)),
                                    DataCell(Text(worker.email)),
                                    DataCell(Text(worker.address ?? 'N/A')),
                                    DataCell(Text(worker.isStore == 1 ? 'Store' : 'Individual')),
                                    DataCell(
                                      TextButton(
                                        onPressed: () {
                                          Get.toNamed('/WorkerDetailsPage', arguments: worker.id);
                                        },
                                        child: Text('More'),
                                      ),
                                    ),
                                  ],
                                );
                              }).toList(),
                            ),
                          );
                        }
                      }),

                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
