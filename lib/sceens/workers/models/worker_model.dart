import 'dart:convert';

CWorker cWorkerFromJson(String str) => CWorker.fromJson(json.decode(str));

String cWorkerToJson(CWorker data) => json.encode(data.toJson());

class CWorker {
  List<DatumWorker> data;

  CWorker({
    required this.data,
  });

  factory CWorker.fromJson(Map<String, dynamic> json) => CWorker(
    data: List<DatumWorker>.from(json["data"].map((x) => DatumWorker.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}
class DatumWorker {
  int? id;
  int? roleId;
  String fullName;
  String email;
  Avatar? avatar;
  String phoneNumber;
  String? address;
  dynamic image;
  int? isStore;

  DatumWorker({
    this.id,
    this.roleId,
    required this.fullName,
    required this.email,
    this.avatar,
    required this.phoneNumber,
    this.address,
    this.image,
    this.isStore,
  });

  factory DatumWorker.fromJson(Map<String, dynamic> json) => DatumWorker(
    id: json["id"] ?? 0,
    roleId: json["role_id"] ?? 0,
    fullName: json["full_name"] ?? '',
    email: json["email"] ?? '',
    avatar: json["avatar"] != null ? avatarValues.map[json["avatar"]] : null,
    phoneNumber: json["phone_number"] ?? '',
    address: json["address"],
    image: json["image"],
    isStore: json["is_store"] ?? 0,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "full_name": fullName,
    "email": email,
    "avatar": avatar != null ? avatarValues.reverse[avatar] : null,
    "phone_number": phoneNumber,
    "address": address,
    "image": image,
    "is_store": isStore,
  };
}

enum Avatar {
  USERS_DEFAULT_PNG
}

final avatarValues = EnumValues({
  "users/default.png": Avatar.USERS_DEFAULT_PNG
});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
