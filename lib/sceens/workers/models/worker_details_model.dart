// To parse this JSON data, do
//
//     final workerDetails = workerDetailsFromJson(jsonString);

import 'dart:convert';

WorkerDetails workerDetailsFromJson(String str) => WorkerDetails.fromJson(json.decode(str));

String workerDetailsToJson(WorkerDetails data) => json.encode(data.toJson());

class WorkerDetails {
  WorkerData data;

  WorkerDetails({
    required this.data,
  });

  factory WorkerDetails.fromJson(Map<String, dynamic> json) => WorkerDetails(
    data: WorkerData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class WorkerData {
  int id;
  int roleId;
  String fullName;
  String email;
  String avatar;
  String phoneNumber;
  String address;
  dynamic image;
  int isStore;

  WorkerData({
    required this.id,
    required this.roleId,
    required this.fullName,
    required this.email,
    required this.avatar,
    required this.phoneNumber,
    required this.address,
    required this.image,
    required this.isStore,
  });

  factory WorkerData.fromJson(Map<String, dynamic> json) => WorkerData(
    id: json["id"],
    roleId: json["role_id"],
    fullName: json["full_name"],
    email: json["email"],
    avatar: json["avatar"],
    phoneNumber: json["phone_number"],
    address: json["address"],
    image: json["image"],
    isStore: json["is_store"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "full_name": fullName,
    "email": email,
    "avatar": avatar,
    "phone_number": phoneNumber,
    "address": address,
    "image": image,
    "is_store": isStore,
  };
}
