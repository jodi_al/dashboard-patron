import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../../confing/confing.dart';
import 'models/worker_model.dart';

class WorkerService extends GetxService {
  var isLoading = true.obs;

  Future<List<DatumWorker>> getWorkers() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }

    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getAllWorkers),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body)['data'];
      print('Workers data: $jsonResponse'); // Debug print
      return jsonResponse.map((worker) => DatumWorker.fromJson(worker)).toList();
    } else {
      throw Exception('Failed to load workers');
    }
  }

  Future<List<DatumWorker>> getTopWorkers() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }

    final response = await http.get(
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.getTopWorkers),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body)['data'];
      print('Top workers data: $jsonResponse'); // Debug print
      return jsonResponse.map((worker) => DatumWorker.fromJson(worker)).toList();
    } else {
      throw Exception('Failed to load top workers');
    }
  }
}
