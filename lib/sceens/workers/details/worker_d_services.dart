// services/worker_details_service.dart
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../../../confing/confing.dart';
import '../models/worker_details_model.dart'; // Ensure the correct path

class WorkerDetailsService extends GetxService {
  var isLoading = true.obs;

  Future<WorkerDetails> getWorkerDetails(int id) async {
    var sharedPreferences = await SharedPreferences.getInstance();
    String? token = sharedPreferences.getString('data');
    if (token == null) {
      Get.snackbar(
        'Error',
        'Token not found. Please login again.',
        snackPosition: SnackPosition.BOTTOM,
      );
      throw Exception('Token not found');
    }

    final response = await http.get(
      Uri.parse('${ServerConfig.domainNameServer + ServerConfig.showWorker}$id'),
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return WorkerDetails.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load worker details');
    }
  }
}
