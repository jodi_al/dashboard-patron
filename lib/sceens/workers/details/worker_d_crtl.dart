// controllers/worker_details_controller.dart
import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/workers/details/worker_d_services.dart';
import '../models/worker_details_model.dart'; // Ensure the correct path

class WorkerDetailsController extends GetxController {
  final WorkerDetailsService workerDetailsService = Get.put(WorkerDetailsService());
  var workerDetails = Rxn<WorkerDetails>();
  var isLoading = false.obs;
  var errorMessage = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> fetchWorkerDetails(int id) async {
    isLoading(true);
    errorMessage('');
    try {
      WorkerDetails details = await workerDetailsService.getWorkerDetails(id);
      workerDetails.value = details;
      print('Worker details fetched: ${workerDetails.value}');
    } catch (e) {
      errorMessage(e.toString());
      print('Error fetching worker details: $e');
    } finally {
      isLoading(false);
    }
  }
}
