// pages/worker_details_page.dart
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/componets/Main_view.dart';
import 'package:patron_dashboard/confing/confing.dart';
import 'package:patron_dashboard/const.dart';
import 'package:patron_dashboard/sceens/workers/details/worker_d_crtl.dart';

class WorkerDetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final int workerId = Get.arguments ?? 0;
    final WorkerDetailsController workerDetailsController = Get.put(WorkerDetailsController());

    // Fetch worker details if they are not already fetched
    if (workerDetailsController.workerDetails.value == null || workerDetailsController.workerDetails.value!.data.id != workerId) {
      workerDetailsController.fetchWorkerDetails(workerId);
    }

    return MainView(
      key: UniqueKey(),
      child: Padding(
        padding: const EdgeInsets.all(defaultPadding),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text(
                'Worker Details',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            body: Obx(() {
              if (workerDetailsController.isLoading.value) {
                return Center(child: CircularProgressIndicator());
              } else if (workerDetailsController.errorMessage.isNotEmpty) {
                return Center(child: Text(workerDetailsController.errorMessage.value));
              } else if (workerDetailsController.workerDetails.value != null) {
                var worker = workerDetailsController.workerDetails.value!.data;
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          worker.avatar.isNotEmpty
                              ? Image.network(
                            '${ServerConfig.domainNameServer}${worker.avatar}',
                            width: 100,
                            height: 100,
                            errorBuilder: (context, error, stackTrace) {
                              return Icon(Icons.account_circle, size: 100);
                            },
                          )
                              : Icon(Icons.account_circle, size: 100),
                          SizedBox(width: 16),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(worker.fullName,
                                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                              Text(worker.email),
                              Text(worker.phoneNumber),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 16),
                      Text('Address: ${worker.address}', style: TextStyle(fontSize: 18)),
                      SizedBox(height: 16),
                      Text('Role ID: ${worker.roleId}', style: TextStyle(fontSize: 18)),
                      SizedBox(height: 16),
                      Text('Is Store: ${worker.isStore == 1 ? 'Yes' : 'No'}', style: TextStyle(fontSize: 18)),
                      TextButton(
                        onPressed: () {
                          Get.offNamed('/WorkersPage');
                        },
                        child: Text(
                          'Go back',
                          style: TextStyle(fontSize: 18, color: purple1),
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(child: Text('Worker not found'));
              }
            }),
          ),
        ),
      ),
    );
  }
}
