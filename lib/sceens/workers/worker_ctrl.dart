import 'package:get/get.dart';
import 'package:patron_dashboard/sceens/workers/worker_services.dart';

import 'models/worker_model.dart';

class WorkerController extends GetxController {
  var workers = <DatumWorker>[].obs;
  var topWorkers = <DatumWorker>[].obs;
  var totalWorkersCount = 0.obs;
  var topWorkersCount = 0.obs;
  var isLoading = true.obs;
  var errorMessage = ''.obs;
  var showingTopWorkers = false.obs;

  @override
  void onInit() {
    fetchWorkers();
    fetchTopWorkers();
    super.onInit();
  }

  void fetchWorkers() async {
    try {
      isLoading(true);
      errorMessage('');
      var workersList = await WorkerService().getWorkers();
      workers.assignAll(workersList);
      totalWorkersCount.value = workersList.length;
    } catch (e) {
      errorMessage(e.toString());
    } finally {
      isLoading(false);
      showingTopWorkers(false);
    }
  }

  void fetchTopWorkers() async {
    try {
      isLoading(true);
      errorMessage('');
      var topWorkersList = await WorkerService().getTopWorkers();
      topWorkers.assignAll(topWorkersList);
      topWorkersCount.value = topWorkersList.length;
    } catch (e) {
      errorMessage(e.toString());
    } finally {
      isLoading(false);
      showingTopWorkers(true);
    }
  }
}
