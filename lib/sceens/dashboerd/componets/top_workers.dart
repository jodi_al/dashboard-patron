import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/const.dart';
import '../../workers/models/worker_model.dart';
import '../../workers/worker_ctrl.dart';

class TopWorkers extends StatelessWidget {
  final WorkerController workerController = Get.put(WorkerController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (workerController.isLoading.value) {
        return Center(child: CircularProgressIndicator());
      } else if (workerController.topWorkers.isEmpty) {
        return Center(child: Text('No top workers available'));
      } else {
        return Column(
          children: [
            ...workerController.topWorkers.asMap().entries.map((entry) {
              int idx = entry.key;
              DatumWorker worker = entry.value ;
              return WorkerRow(
                id: (idx + 1).toString(),
                name: worker.fullName,
                orders: '50', // You might want to replace this with actual data
                color: getColorByIndex(idx),
              );
            }).toList(),
          ],
        );
      }
    });
  }

  Color getColorByIndex(int index) {
    switch (index % 4) {
      case 0:
        return pink;
      case 1:
        return orange;
      case 2:
        return green;
      case 3:
        return lighPurple;
      default:
        return pink;
    }
  }
}

Widget topWorkerHeader() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        '#',
        style: TextStyle(color: gray),
      ),
      Text(
        'Name',
        style: TextStyle(color: gray),
      ),
      Text(
        'Total Orders',
        style: TextStyle(color: gray),
      ),
    ],
  );
}

class WorkerRow extends StatelessWidget {
  final String id;
  final String name;
  final String orders;
  final Color color;

  WorkerRow({required this.name, required this.orders, required this.color, required this.id});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            id,
            style: TextStyle(fontSize: 16),
          ),
          Text(
            name,
            style: TextStyle(fontSize: 16),
          ),
          Text(
            orders,
            style: TextStyle(fontSize: 16, color: color),
          ),
        ],
      ),
    );
  }
}
