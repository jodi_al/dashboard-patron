import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class CustomerSatisfactionChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: LineChart(
        LineChartData(borderData: FlBorderData(show: false),
          titlesData: FlTitlesData(show: false, ),
          lineBarsData: [
            LineChartBarData(
              spots: [
                FlSpot(0, 3),
                FlSpot(1, 2),
                FlSpot(2, 5),
                FlSpot(3, 3.1),
                FlSpot(4, 4),
                FlSpot(5, 3),
                FlSpot(6, 4),
              ],
              isCurved: true,
              color: Colors.blue,
              barWidth: 4,
              belowBarData: BarAreaData(
                 color:
                Colors.blue.withOpacity(0.3),
              ),
            ),
            LineChartBarData(
              spots: [
                FlSpot(0, 4),
                FlSpot(1, 3),
                FlSpot(2, 4),
                FlSpot(3, 3.8),
                FlSpot(4, 5),
                FlSpot(5, 4),
                FlSpot(6, 5),
              ],
              isCurved: true,
              color: Colors.green,
              barWidth: 4,
              belowBarData: BarAreaData(show: true, color:
                Colors.green.withOpacity(0.3),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class VisitorInsightsChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: LineChart(
        LineChartData(borderData: FlBorderData(show: false),
          titlesData: FlTitlesData(
            topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false),),
            rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false),),

            leftTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 40,
                interval: 100,

                getTitlesWidget: (value, meta) {
                  switch (value.toInt()) {
                    case 0:
                      return Text('0');
                    case 1:
                      return Text('100');
                    case 2:
                      return Text('200');
                    case 3:
                      return Text('300');
                    case 4:
                      return Text('400');
                    default:
                      return Container();
                  }
                },
              ),
            ),
            bottomTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                getTitlesWidget: (value, meta) {
                  const style = TextStyle(
                    color: Color(0xff68737d),
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                  );
                  Widget text;
                  switch (value.toInt()) {
                    case 0:
                      text = Text('Jan', style: style);
                      break;
                    case 1:
                      text = Text('Feb', style: style);
                      break;
                    case 2:
                      text = Text('Mar', style: style);
                      break;
                    case 3:
                      text = Text('Apr', style: style);
                      break;
                    case 4:
                      text = Text('May', style: style);
                      break;
                    case 5:
                      text = Text('Jun', style: style);
                      break;
                    case 6:
                      text = Text('Jul', style: style);
                      break;
                    case 7:
                      text = Text('Aug', style: style);
                      break;
                    case 8:
                      text = Text('Sep', style: style);
                      break;
                    case 9:
                      text = Text('Oct', style: style);
                      break;
                    case 10:
                      text = Text('Nov', style: style);
                      break;
                    case 11:
                      text = Text('Dec', style: style);
                      break;
                    default:
                      text = Text('', style: style);
                      break;
                  }
                  return SideTitleWidget(
                    axisSide: meta.axisSide,
                    child: text,
                  );
                },
              ),
            ),
          ),
          lineBarsData: [
            LineChartBarData(
              spots: [
                FlSpot(0, 1),
                FlSpot(2, 2.5),
                FlSpot(4, 3.2),
                FlSpot(6, 3.5),
                FlSpot(8, 4),
                FlSpot(9, 5),
                FlSpot(10, 4.2),
              ],
              isCurved: true,
              color: Colors.purple,
              barWidth: 4,
            ),
            LineChartBarData(
              spots: [
                FlSpot(0, 2),
                FlSpot(1, 2.5),
                FlSpot(2, 3),
                FlSpot(4, 3.5),
                FlSpot(5, 4),
                FlSpot(6, 4.2),
                FlSpot(8, 5),
                FlSpot(9, 4.8),
                FlSpot(11, 5.5),
              ],
              isCurved: true,
              color: Colors.red,
              barWidth: 4,
            ),
            LineChartBarData(
              spots: [
                FlSpot(0, 1.5),
                FlSpot(1, 2),
                FlSpot(3, 2.8),
                FlSpot(4, 3),
                FlSpot(5, 3.2),
                FlSpot(6, 3.5),
                FlSpot(7, 4),
                FlSpot(8, 3.8),
                FlSpot(11, 4.8),
              ],
              isCurved: true,
              color: Colors.green,
              barWidth: 4,
            ),
          ],
        ),
      ),
    );
  }
}