import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/const.dart';
import '../../products/models/top_product_model.dart';
import '../../products/top_product/crtl.dart';

class TopProducts extends StatelessWidget {
  final TopProductController topProductController = Get.put(TopProductController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (topProductController.isLoading.value) {
        return Center(child: CircularProgressIndicator());
      } else if (topProductController.topProducts.isEmpty) {
        return Center(child: Text('No top products available'));
      } else {
        return Column(
          children: [
            ...topProductController.topProducts.asMap().entries.map((entry) {
              int idx = entry.key;
              TopProductDetails product = entry.value;
              return ProductRow(
                id: (idx + 1).toString(),
                name: product.name,
                popularity: product.isSold,
                sales: product.name,
                color: getColorByIndex(idx),
              );
            }).toList(),
          ],
        );
      }
    });
  }

  Color getColorByIndex(int index) {
    switch (index % 4) {
      case 0:
        return pink;
      case 1:
        return orange;
      case 2:
        return green;
      case 3:
        return lighPurple;
      default:
        return pink;
    }
  }
}

Widget topProductHeader() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Expanded(
        
        child: Text(
          '#',
          style: TextStyle(color: gray),
        ),
      ),
      Expanded(
        flex: 2,
        child: Text(
          'Name',
          style: TextStyle(color: gray),
        ),
      ),
      Expanded(
        flex: 3,
        child: Text(
          'Popularity',
          style: TextStyle(color: gray),
        ),
      ),
      const SizedBox(width: 20),
      Expanded(
        flex: 2,
        child: Text(
          'Sales',
          style: TextStyle(color: gray),
        ),
      ),
    ],
  );
}

class ProductRow extends StatelessWidget {
  final String id;
  final String name;
  final int popularity;
  final String sales;
  final Color color;

  ProductRow({
    required this.name,
    required this.popularity,
    required this.sales,
    required this.color,
    required this.id,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(id),
            ),
            Expanded(
              flex: 2,
              child: Text(
                name,
                style: TextStyle(fontSize: 16),
              ),
            ),
            Expanded(
              flex: 3,
              child: LinearProgressIndicator(
                value: popularity / 100,
                color: color,
                backgroundColor: Colors.blue.withOpacity(0.2),
              ),
            ),
            const SizedBox(width: 20),
            Expanded(
              flex: 2,
              child: Text(
                sales,
                style: TextStyle(fontSize: 16, color: color),
                // textAlign: TextAlign.end,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
