import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:patron_dashboard/const.dart';

class SummaryCard extends StatelessWidget {
  final String title;
  final String subtitle;
  final String percentageChange;
  final Color color;
  final String svgSrc;


  SummaryCard({
    required this.title,
    required this.subtitle,
    required this.percentageChange,
    required this.color,
    required this.svgSrc,

  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Card(
        shadowColor: gray,
        margin: EdgeInsets.all(16),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12)
        ),
        color: color,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset(
                svgSrc,
                color: Colors.white,
               // colorFilter: ColorFilter.mode(color.withOpacity(0.5), BlendMode.srcIn),
                height: 24,
              ),
              Text(
                title,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.blue[900]),
              ),
              SizedBox(height: 5),
              Text(
                subtitle,
                style: TextStyle(fontSize: 16, color: Colors.grey[800]),
              ),
              SizedBox(height: 5),
              Text(
                percentageChange,
                style: TextStyle(fontSize: 12, color: Colors.blue),
              ),
            ],
          ),
        ),
      ),
    );
  }
}