import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:patron_dashboard/const.dart';
import '../../componets/Main_view.dart';
import '../../responseive.dart';
import 'componets/charts.dart';
import 'componets/summray_cards.dart';
import 'componets/top_peoducts.dart';
import 'componets/top_workers.dart';

class DashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Responsive(
      // Define different layouts for mobile, tablet, and desktop
      mobile: buildMobileLayout(),
      tablet: buildTabletLayout(),
      desktop: buildDesktopLayout(),
    );
  }

  Widget buildMobileLayout() {
    return MainView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: buildContent(),
          ),
        ),
      ),
    );
  }

  Widget buildTabletLayout() {
    return MainView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: buildContent(),
          ),
        ),
      ),
    );
  }

  Widget buildDesktopLayout() {
    return MainView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: buildContent(),
          ),
        ),
      ),
    );
  }

  List<Widget> buildContent() {
    return [
      // Today's Sales Summary
      Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          padding: EdgeInsets.all(30),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Today's Sales",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SummaryCard(
                    svgSrc: 'assets/icons/chat-square-2.svg',
                    title: '\$1k',
                    subtitle: 'Total Sales',
                    percentageChange: '+8% from yesterday',
                    color: pink,
                  ),
                  SummaryCard(
                    svgSrc: 'assets/icons/menu_doc.svg',
                    title: '300',
                    subtitle: 'Total Orders',
                    percentageChange: '+5% from yesterday',
                    color: orange,
                  ),
                  SummaryCard(
                    svgSrc: 'assets/icons/tag.svg',
                    title: '5',
                    subtitle: 'Product Sold',
                    percentageChange: '+1.2% from yesterday',
                    color: green,
                  ),
                  SummaryCard(
                    svgSrc: 'assets/icons/menu_profile.svg',
                    title: '8',
                    subtitle: 'New Customers',
                    percentageChange: '0.5% from yesterday',
                    color: lighPurple,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      // SizedBox(height: 20),
      // Top Products and Top Workers
      Padding(

        padding: const EdgeInsets.all(20.0),
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Top Products",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    topProductHeader(),
                    TopProducts(),
                  ],
                ),
              ),
            ),
            const SizedBox(width: 30),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Top Workers",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    topWorkerHeader(),
                    TopWorkers(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      // SizedBox(height: 20),
      // Charts Section
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Customer Satisfaction",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    CustomerSatisfactionChart(),
                  ],
                ),
              ),
            ),
            SizedBox(width: 20),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Visitor Insights",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    VisitorInsightsChart(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ];
  }
}
