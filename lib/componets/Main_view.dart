import 'package:flutter/material.dart';
import 'package:patron_dashboard/componets/header.dart';
import 'package:patron_dashboard/componets/side_menu.dart';

import '../const.dart';
import '../controller/MenuAppController.dart';
import '../responseive.dart';

class MainView extends StatelessWidget {
  const MainView({super.key, required this.child});

  final Widget child;

  @override
  Widget build(context) {
    return Scaffold(
      //key:        context.read<MenuAppController>().scaffoldKey,

      /// You can use an AppBar if you want to
      appBar: AppBar(
        automaticallyImplyLeading: !Responsive.isDesktop(context), // Automatically show the menu button on non-desktop
        title: navBar(context),
        elevation: 0,
        
        backgroundColor: purple,
        centerTitle: true,
      ),
      drawer: Responsive.isDesktop(context) ? null : SideMenu(), // Include the drawer for non-desktop devices
      body: Row(
        children: [
          if (Responsive.isDesktop(context))
            Expanded(flex: 1, child: SideMenu()),
          /// Make it take the rest of the available width
          Expanded(flex: 5, child: child),
        ],
      ),
    );
  }
}

Widget navBar(context) {
  return Container(
    color: purple, // Set the background color for the NavBar
    child: Row(
      children: [
        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 1 : 1), // Spacing between the text and the search field

        // Expanded(
        //   flex: Responsive.isDesktop(context) ? 1 : 2,
        //   child: Text(
        //     'DashBoard',
        //     style: TextStyle(
        //         color: Colors.white,
        //         fontStyle: FontStyle.normal,
        //         fontWeight: FontWeight.normal,
        //         fontSize: 20),
        //   ),
        // ),
        const SizedBox(width: 120),
        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 2 : 2), // Spacing between the text and the search field

        Expanded(
          flex: 3, // Adjust flex factor as needed
          child: Padding(
            padding: const EdgeInsets.all(30),
            child: SearchField(),
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.notifications),
          color: Colors.white,
        ),
        Row(
          children: [
            SizedBox(
              height: 40,
              child: Image.asset('assets/images/profile.jpg'),
            ),
            const Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Roaa',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                  Text(
                    'admin',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ],
              ),
            ),
          ],
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.arrow_drop_down_sharp),
          color: light_purple,
        ),
      ],
    ),
  );
}
