import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:patron_dashboard/const.dart';


class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor:purple ,
      // shadowColor: Colors.white,
      // surfaceTintColor: Colors.white,
      shape: Border(bottom: BorderSide.none),
      elevation: 2,
      child: ListView(
        children: [
          DrawerHeader(
            // decoration: BoxDecoration(
            //   color: purple,
            // ),
            margin: EdgeInsets.zero,

            child: Image.asset("assets/images/logo.png"),
          ),
          DrawerListTile(
            title: "Dashboard",
            svgSrc: "assets/icons/menu_task.svg",
            press: () {
              Get.toNamed('/DashPage');
            },
          ),
          DrawerListTile(
            title: "Customers",
            svgSrc: "assets/icons/menu_profile.svg",
            press: () {  Get.toNamed('/CustomersPage');},
          ),
          DrawerListTile(
            title: "Workers",
            svgSrc: "assets/icons/menu_profile.svg",
            press: () {  Get.toNamed("/WorkersPage");},
          ),
          DrawerListTile(
            title: "Orders",
            svgSrc: "assets/icons/menu_doc.svg",
            press: () {  Get.toNamed( '/OrdersPage');},
          ),
          DrawerListTile(
            title: "Products",
            svgSrc: "assets/icons/menu_store.svg",
            press: () {  Get.toNamed('/ProductsPage');},
          ),
          DrawerListTile(
            title: "Offers",
            svgSrc: "assets/icons/menu_notification.svg",
            press: () {  Get.toNamed('/OffersPage');},
          ),
          DrawerListTile(
            title: "Reports",
            svgSrc: "assets/icons/menu_dashboard.svg",
            press: () {  Get.toNamed('/ReportsPage');},
          ),
          DrawerListTile(
            title: "Messages",
            svgSrc: "assets/icons/chat.svg",
            press: () {  Get.toNamed('/MassagePage');},
          ),
          DrawerListTile(
            title: "Settings",
            svgSrc: "assets/icons/menu_setting.svg",
            press: () {  Get.toNamed('/SettingPage');},
          ),
          DrawerListTile(
            title: "Sign out",
            svgSrc: "assets/icons/menu_setting.svg",
            press: () {   Get.toNamed('/LogOutPage'); },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      
      // horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        colorFilter: ColorFilter.mode(Colors.white, BlendMode.srcIn),
        height: 16,
      ),
      title: Text(
        title,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}

