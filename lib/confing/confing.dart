class ServerConfig{
  static const domainNameServer ="http://127.0.0.1:8000";

  //auth
  static const login = '/api/login';

  //product
  static const createProduct ='/api/create/products';
  static const getAllProduct ='/api/products';
  static const showProductDetailes ='/api/showProduct/';
  static const deleteProduct ='/api/products/';
  static const topProduct ='/api/products/top';

  //details
  static const getColors ='/api/getAllColors';
  static const getSizes ='/api/getAllSizes';
  static const getShapes ='/api/getAllShapes';
  static const getAllCategories ='/api/getAllCategories';
  static const getAllFabrics ='/api/fabrics';

 //customers
  static const getAllCustomers ='/api/getAllCustomers';
  static const showCustomer ='/api/showCustomer/';

  //workers
  static const getAllWorkers='/api/getAllWorkers';
  static const showWorker='/api/showWorker/';
  static const createWorker='/api/createWorker';
  static const getTopWorkers='/api/workers/top';

   //Orders
  static const AllOrderForAdmin='/api/AllOrderForAdmin';
  static const ShowOrder='/api/ShowOrder/';
  static const ConfirmOrder='/api/ConfirmOrder';

  //offers
  static const getOffers='/api/getAllOffers';
  static const insertOffers='/api/InsertOffer';



}