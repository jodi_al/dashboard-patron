

import 'package:patron_dashboard/confing/storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

setHeaders({bool useToken = false}) {
  if (useToken) {
    return {
      'content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':
          'Bearer ${storage.get<SharedPreferences>().getString("token")}'
    };
  } else {}
}
